# Testing Strategy
The functional structure of akira is as follow
f(x) -> state change, x is from database

In command pattern we generaly modeling the f(x) -> new state part, and implement a persistent data structure.

And we need to dump data

## Unit test

1. test only one file
2. using interface to mock the external module.
3. Make sure that External Module is called.

### What to test

1. Function Call, Expected-Result Test Pattern

What is the down-side?

Interface Changed, and need to rewrite whole test?

1. Easy Refactor?, we need to recreate the class fixture

Mock all metaclass|interface,

Service
Command Topic
     [Command1 ->  Command2 -> Command3]

Entity topic
    [x0, x1.....x_{t+1}]

Transaction

Since Command is ordered
Entity Topic is ordered

Order Service

1. RESTAPI(Need for response)|Since We need to keep the Order Service State 
strictly "Ordered", we only use one Source to Topic 

PNL Service
1. subscribe to balance topic, and market quote

# Development Log  

## Trade Management

using the akira client we can easily management our trading position. AKIRA 
suport various ticker to trade. And adding one need to define in the 
akira/services/positions/domain/trade, and the ticker need be define a source, 
which can be update by the akira.

```python

res = awiat client.submit_trade(account\_id="mv", 
                    symbol="USDJPY INVT Curncy", unit=10)

```
2021.08.02 
there is a bug in fastapi, response\_model cannot use the typing.List, see issue 
https://github.com/tiangolo/fastapi/issues/1504

### Execution Price

The Restriction Symbol is done this part is read to implement.
And by default the execution will be availible at end of trading time, this 
can be config at the configuration(execution).

### Posiation

Position only valid when a trade is executed successfully.

### Core
Probability need to refactor how the system build, since we are using some framwork in the develement proccess, in order to access the root service instance we need to import the root\_service in the function body, to aviod the recursive import(domain/{doamin\_model}/streams.py), its because we need to access the service.


