from core.functools.multimethod import multimethod
from dataclasses import dataclass


def test_simple_dispatch():

    @multimethod
    def function1(a: int, b: int):
        return a + b

    @function1.register
    def _(a: str, b: str):
        return a + b

    assert function1(1, 1) == 2
    assert function1("1", "1") == "11"


def test_dispatch_method():

    @dataclass
    class A:
        c: int

        @multimethod
        def function1(self, a: int, b: int):
            return a + b + self.c

        @function1.register
        def _(self, a: str, b: str):
            c = str(self.c)
            return a + b + c
    a = A(c=1)

    assert a.function1(1, 1) == 3
    assert a.function1("1", "1") == "111"


def test_dispatch_methoddiscriptor():

    @dataclass
    class A:
        c: int
        d = 1

        @multimethod
        @classmethod
        def function1(cls, a: int, b: int):
            return a + b + cls.d

        @function1.register
        def _(cls, a: str, b: str):
            c = str(cls.d)
            return a + b + c

    assert A.function1(1, 1) == 3
    assert A.function1("1", "1") == "111"
    b = A(c=3)
    assert b.function1(1, 1) == 3
    assert b.function1("1", "1") == "111"

    @dataclass
    class B(A):
        d: int

    assert B.function1(1, 1) == 3
    assert B.function1("1", "1") == "111"
    b = B(c=3, d=1)
    assert b.function1(1, 1) == 3
    assert b.function1("1", "1") == "111"

