
def test_multi_index_store():
    from core.stores.base import MultiIndexStore
    key = ("1", "2", "4")
    value = 1
    MultiIndexStore.dump(key, value)
    key = ("1", "2", "3")
    MultiIndexStore.dump(key, value)
    assert MultiIndexStore.load(key) == value
    assert MultiIndexStore.load(("1", )) == {"2": {"4": value, "3": value}}
