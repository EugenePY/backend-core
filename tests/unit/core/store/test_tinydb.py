import pytest
from core.stores.tinydb import TinyDBStore
from domain.user import UserAggregate
from pydantic.json import pydantic_encoder
from tinydb import TinyDB


@pytest.fixture
def mock_db(tmp_path):
    db = TinyDB(tmp_path/"test.json", default=pydantic_encoder)
    yield db


@pytest.fixture
def setup_aggregate_store(mock_db):
    class MyStore(TinyDBStore, db=mock_db,
                  aggregate_type=UserAggregate):
        ...

    agg, event = UserAggregate.create(user_id="sdf", name="sdf",
                                      email="asdf@gmail.com",
                                      password="sdsdfsdf")
    yield agg, event, MyStore


def test_load(mocker, logger, setup_aggregate_store):
    agg, _, store_cls = setup_aggregate_store
    logger.debug(store_cls.dump(agg.id, agg))
    obj = store_cls.load(agg.id)
    assert obj == agg
