import pytest
from core.stores.timeseries import TimeSeriesStore
from core.record.timeseries.record import (
    SeriesId, TimeIndexedRecord)
from core.stores.timeseries.entity import TimeSeriesBatch
from dataclasses import dataclass, asdict
import datetime
from core.schema import pydantic_basemodel, cast
from pydantic import BaseModel


@pytest.fixture
def setup_timeseries_data():
    import uuid
    series_id = SeriesId.from_string("a:b")

    @pydantic_basemodel
    @dataclass
    class A(TimeIndexedRecord):
        a: float

    time = datetime.datetime(2020, 1, 1)
    record = A(timestamp=time, a=1.)

    store = type("TestingStore", (TimeSeriesStore,), {}, max_length=10)
    yield series_id, record, store


def test_timeseries_diff(logger, setup_timeseries_data):
    series_id, record, store = setup_timeseries_data
    events = store.diff(series_id, record)
    assert len(events) == 2
    for event in events:
        store = store.apply(series_id, event)


def test_timeseries_dump(logger, setup_timeseries_data):
    series_id, record, store = setup_timeseries_data
    store.dump(series_id, record)
    loaded = store.load(series_id)
    assert loaded[record.timestamp].a == 1.


def test_timeseries_dataframe(logger, setup_timeseries_data):
    series_id, record, store = setup_timeseries_data

    store.dump(series_id, record)
    loaded = store.load(series_id)
    data = loaded.as_dataframe()
    data = data.reset_index()
    logger.debug(data)
    deserialized = TimeSeriesBatch.from_dataframe(
        series_id, data, record.from_jsonable, max_length=10)
    assert deserialized == loaded


def test_timeseries_record(logger, setup_timeseries_data):
    series_id, record, store = setup_timeseries_data
    store.dump(series_id, record)
    loaded = store.load(series_id)
    data = loaded.as_records()
    logger.debug(data)
    deserialized = TimeSeriesBatch.from_records(
        series_id, data, record.from_jsonable, max_length=10)
    assert deserialized == loaded

