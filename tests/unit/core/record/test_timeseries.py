from core.record.timeseries import (
    TimeSeriesBatch, SeriesId)
from core.stores.timeseries.entity import TimeIndexedRecord
import datetime
from functools import reduce
from dataclasses import dataclass
import uuid

def datetime_range(start, n, timedelta):
    for i in range(n):
        yield start + i * timedelta


def test_timeseries_create():

    _id = SeriesId.from_string("EURUSD:PX_LAST")
    batch_ = TimeSeriesBatch(id=_id, data={})
    batch = reduce(lambda x, y: x.create_index(y, 1), [i for i in datetime_range(
                                    datetime.datetime(2020, 1, 1), 10,
                                    datetime.timedelta(days=1))], batch_)
    new_batch = batch.create_index(datetime.datetime(2020, 1, 27), 1)



def test_audit():
    _id = SeriesId.from_string("EURUSD:PX_LAST")
    batch = TimeSeriesBatch(id=_id, data={})

    @dataclass
    class B(TimeIndexedRecord):
        a: int

    new_batch = TimeSeriesBatch(id=_id,
                                data={t: B(timestamp=t, a=1) for t in datetime_range(
                                    datetime.datetime(2020, 1, 1), 10,
                                    datetime.timedelta(days=1))})
    result = reduce(lambda x, e: x.apply(e),
                    list(batch.diff(new_batch)), batch)
    assert result.data == new_batch.data
    b = B(timestamp=datetime.datetime(2020, 1, 1), a=3)
    result.diff_one(idx=b.timestamp, value=b)
