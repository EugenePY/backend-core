from core.entity import Id, Entity
from dataclasses import dataclass


def test_hash():
    _id = Id()
    _id2 = Id()

    assert _id != _id2
    assert _id == _id


def test_inherent():
    @dataclass(eq=True, frozen=True)
    class MyId(Id):
        ...

    hash(MyId())


def test_entity_inherent():
    @dataclass
    class MyEnity(Entity):
        my_field: int

    entity = MyEnity(id=None, my_field=1)
