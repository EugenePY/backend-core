from core.registery import Registery
import abc



def test_registery(logger):

    class MyRegistery(Registery):
        ...

    class ABSClass:
        def __init_subclass__(cls):
            MyRegistery.register(cls)

        @abc.abstractmethod
        def do(self):
            raise NotImplementedError

    class MyClass(ABSClass):
        def do(self):
            print("doing")

    logger.info(MyRegistery.registered_cls())
    assert MyClass in MyRegistery.registered_cls()
    assert ABSClass not in MyRegistery.registered_cls()
