from core.record.timeseries import SeriesId, TimeIndexedRecord
from dataclasses import dataclass
import datetime


def test_hash():
    series_id = SeriesId.from_string("A:B")

    @dataclass(eq=False, frozen=True)
    class A(SeriesId):
        ...

    hash(A.from_string("A:B"))


def test_index():
    record = TimeIndexedRecord(timestamp=datetime.datetime(2020, 1, 1))


def test_json_serializable():
    from fastapi.encoders import jsonable_encoder
    jsonable_encoder({"d": datetime.datetime(2020, 1, 1)})
