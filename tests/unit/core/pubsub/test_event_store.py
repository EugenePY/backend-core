import pytest
from core.pubsub.event_store import EventStore


@pytest.fixture
def finalize_event_store():
    def _(message_manager):
        event_store = EventStore(message_manager=message_manager)
        event_store.finalize()
        return event_store
    return _


@pytest.mark.asyncio
async def test_dump(produce_events):
    event_store, topic, events = produce_events
    agg_type = topic.agg_type
    assert event_store.events == events

