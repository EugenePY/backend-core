import pytest
from core.pubsub.event_store import EventRecord, TinyDBEventStore
from .conftest import Agg1Created


@pytest.fixture
def finalize_event_store(tinydb, logger):
    from core.aggregate import AggregateRegistery
    from core.stores.tinydb import tinydb_factory
    from core.stores.memory import memory_store_factory
    from core.events import EventRegistery

    AggregateRegistery.set_repo_cls(tinydb_factory, db=tinydb)

    def _(message_manager):
        event_store = TinyDBEventStore(
            db=tinydb, message_manager=message_manager)
        event_store.finalize()
        return event_store
    yield _
    AggregateRegistery.set_repo_cls(memory_store_factory)


def test_store_event(tinydb, logger):
    offset = 0
    record = EventRecord(id="sdfsdf", producer_id="sdf",
                         event=Agg1Created(agg_id="sdfsdf", a=1),
                         offset=offset)
    store = TinyDBEventStore(db=tinydb, message_manager=None)
    store.store_event(record)
    logger.debug(store.db.all())
    loaded_record = store.load_event(offset=offset)
    assert record == loaded_record


@pytest.mark.asyncio
async def test_tinydb_event_store(produce_events):
    event_store, topic, events = produce_events
    print(event_store)
    # event_store.dump_repo()
    # print(event_store.db.tables())
