import pytest
from core.pubsub.producers import Topic
from core.pubsub.event_store import Event
from dataclasses import dataclass
from core.command import apply
from core.aggregate import Aggregate
from core.functools import singledispatchmethod, functional
from core.command import command
from core.schema import pydantic_basemodel
import typing


@dataclass
class Agg1Created(Event):
    agg_id: str
    a: int


@dataclass
class Agg1Updated(Event):
    value: int


@dataclass
class Agg1Deleted(Event):
    ...


@pydantic_basemodel
@dataclass
class MyAgg1(Aggregate):
    id: str
    a: int

    @singledispatchmethod
    @classmethod
    def initial_apply(cls, event: typing.Any):
        raise NotImplementedError

    @initial_apply.register
    @classmethod
    def _(cls, event: Agg1Created):
        return cls(id=event.agg_id, a=event.a)

    @singledispatchmethod
    def apply(self, event: typing.Any):
        raise NotImplementedError

    @apply.register
    @functional
    def _(self, event: Agg1Updated):
        self.a = event.value
        return self

    @apply.register
    @functional
    def _(self, event: Agg1Deleted):
        return None

    @command
    @classmethod
    def create(cls, agg_id: str, a: int):
        return Agg1Created(agg_id=agg_id, a=a)

    @command
    def update(self, value: int):
        return Agg1Updated(value=value)

    @command
    def delete(self):
        return Agg1Deleted()

    @staticmethod
    def get_id_from_foreign(foreign):
        return foreign.agg


@pytest.fixture
def tinydb(tmp_path):
    from pydantic.json import pydantic_encoder
    from tinydb import TinyDB
    yield TinyDB(tmp_path / "test.json", default=pydantic_encoder)


@pytest.fixture
@pytest.mark.asyncio
async def setup_event_store(finalize_event_store, event_loop):
    from core.pubsub import MessageManager, Consumer

    topic = Topic(id="test", agg_type=MyAgg1)
    consumer = Consumer(id="test-consumer")

    manager = MessageManager(loop=event_loop)

    event_store = finalize_event_store(manager)

    @consumer.listen(topic)
    async def proccess_topics(id, event):
        agg = MyAgg1.repo.load(id)
        new_agg = apply(MyAgg1, agg, event)
        MyAgg1.repo.dump(id, new_agg)

    await manager.start()
    yield event_store, topic
    await manager.stop()


@pytest.fixture
async def produce_events(setup_event_store):
    event_store, topic = setup_event_store
    agg_type = topic.agg_type
    _id = "sdf"
    agg, event = agg_type.create(agg_id="sdf", a=10)
    agg1, event1 = agg.update(value=30)
    agg2, event2 = agg1.delete()
    events = [event, event1, event2]
    for event in events:
        await topic.publish(id=_id, event=event)
    yield event_store, topic, events


