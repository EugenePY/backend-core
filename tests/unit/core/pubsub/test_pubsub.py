from core.pubsub import Consumer, Producer, MessageManager
from core.command import apply
from core.aggregate import Aggregate
from core.events import Event
from core.functools import singledispatchmethod, functional
from dataclasses import dataclass
import pytest
from multimethod import multimethod
import typing
from logging import getLogger
import asyncio

logger = getLogger(__name__)

Publisher = Producer


@dataclass
class Agg1Created(Event):
    a: int


@dataclass
class Agg1Updated(Event):
    value: int


@dataclass
class Agg1Deleted(Event):
    ...


@dataclass
class MyAgg1(Aggregate):
    a: int
    agg2: str = None

    @singledispatchmethod
    @classmethod
    def initial_apply(cls, event: typing.Any):
        raise NotImplementedError

    @initial_apply.register
    @classmethod
    def _(cls, event: Agg1Created):
        return cls.create(_id=event._id, a=event.a)

    @singledispatchmethod
    def apply(self, event: typing.Any):
        raise NotImplementedError

    @apply.register
    @functional
    def _(self, event: Agg1Updated):
        return self.update(value=event.value)

    @multimethod
    def listen(self, _id: typing.Any, event: typing.Any):
        raise NotImplementedError

    @listen.register
    @functional
    def _(self, _id: str, event: "Agg2Created"):
        if self.agg2 is None:
            self.agg2 = _id
        return self

    @classmethod
    def create(cls, _id: str, a: int):
        return cls(id=_id, a=a)

    def update(self, value: int):
        self.a = value
        return self

    @staticmethod
    def get_id_from_foreign(foreign):
        return foreign.agg


@dataclass
class Agg2Created(Event):
    a: int


@dataclass
class Agg2Deleted(Event):
    ...


@dataclass
class MyAgg2(Aggregate):
    b: str
    agg: str

    @singledispatchmethod
    @classmethod
    def initial_apply(cls, event: typing.Any):
        raise NotImplementedError

    @initial_apply.register
    @classmethod
    def _(cls, event: Agg2Created):
        return cls.create(id=event.agg2_id, b=event.b, agg=event.agg)

    @singledispatchmethod
    def apply(self, event: typing.Any):
        raise NotImplementedError

    @multimethod
    def listen(self, _id: typing.Any, event: typing.Any):
        raise NotImplementedError

    @listen.register
    def _(self, _id: str, event: Agg1Deleted):
        return self.delete()

    @classmethod
    def create(cls, _id: str, b: str, agg: str):
        return cls(id=_id, b=b, agg=agg)

    def delete(self):
        return None

    @staticmethod
    def get_id_from_foreign(foreign):
        return foreign.agg2


class publish:
    """
    publish decorator
    """
    def __init__(self, producer):
        self.producer = producer

    def __call__(self, fn):
        self.fn = fn
        return self

    def __get__(self, obj, cls=None):
        async def _method(*arg, **kwargs):
            id, event = self.fn.__get__(obj, cls)(*arg, **kwargs)
            await self.producer.publish(id, event)
            return id, event

        return _method


class Agg1Service:
    aggregate_type = MyAgg1
    state_events = Producer(id="agg1-request")

    @publish(state_events)
    @classmethod
    def create(cls, agg1_id: str, a: int):
        agg = cls.aggregate_type.repo.load(agg1_id)
        if agg is None:
            event = Agg1Created(a=a)
            agg_new = apply(cls.aggregate_type, agg, event)
            cls.aggregate_type.repo.dump(agg1_id, agg_new)
            return agg1_id, event
        return agg1_id, None

    @publish(state_events)
    @classmethod
    def update_value(cls, agg1_id: str, value: int):
        agg = cls.aggregate_type.repo.load(agg1_id)
        if agg is not None:
            return agg1_id, Agg1Updated(value=value)
        return agg1_id, None

    @publish(state_events)
    @classmethod
    def delete(cls, agg1_id: str):
        agg = cls.aggregate_type.repo.load(agg1_id)
        if agg is not None:
            return agg1_id, Agg1Deleted()
        return agg1_id, None


class Agg2Service:
    aggregate_type = MyAgg2
    request_publisher = Producer(id="agg2-events")
    agg2_consumer = Consumer(id="agg2-agg1-consumer")

    agg1 = set()

    @classmethod
    async def create(cls, agg2_id: str, agg1_id: str, b: str):
        """
        send and wait for results
        """
        event = Agg2Created(agg2_id=agg2_id, agg=agg1_id, b=b)
        await cls.request_publisher.publish(agg2_id, event)
        return agg2_id, event

    @classmethod
    async def create_soon(cls, agg2_id: str, agg1_id: str, b: str):
        """
        only send event
        """
        event = Agg2Created(agg2_id=agg2_id, agg=agg1_id, b=b)
        await cls.build_apply.cast(key=agg2_id, value=event)
        return agg2_id, event

    @classmethod
    async def delete(cls, agg1_id: str):
        agg = cls.aggregate_type.repo.load(agg1_id)
        if agg is not None:
            return agg1_id, Agg1Deleted()
        return agg1_id, None


@pytest.mark.asyncio
async def test_publisher_subs(event_loop):
    agg1 = MyAgg1
    new_id = agg1.repo.gen_id()
    producer = Producer(id="test")
    producer2 = Producer(id="test2")

    consumer = Consumer(id="test-consumer")
    consumer2 = Consumer(id="test-consumer2")

    @consumer.listen(producer)
    async def echo(id, event):
        logger.info(f"consumer={id}, {event}")
        await producer2.publish(id, event.a + 1)

    @consumer2.listen(producer)
    async def echo2(id, event):
        logger.info(f"consumer2={id}, {event}")

    @consumer2.listen(producer2)
    async def echo3(id, event):
        logger.info(f"consumer2={id}, {event}")

    manager = MessageManager(loop=event_loop)
    await manager.start()

    event = Agg1Created(a=1)
    await producer.publish(id=new_id, event=event)
    assert len(producer.subscribers) == 2
    # stop all services
    await manager.stop()


@pytest.mark.asyncio
async def test_stream(event_loop):
    manager = MessageManager(loop=event_loop)

    @Agg2Service.agg2_consumer.listen(Agg1Service.state_events)
    async def agg1_exist(id, event):
        if isinstance(event, Agg1Created):
            Agg2Service.agg1.add(id)
        elif isinstance(event, Agg1Deleted):
            Agg2Service.agg1.remove(id)

    await manager.start()
    await Agg1Service.create(agg1_id=1, a=1)
    obj = Agg1Service.aggregate_type.repo.load(1)
    assert obj.a == 1
    await asyncio.sleep(0.1)
    await manager.stop()
    assert 1 in Agg2Service.agg1


@pytest.mark.asyncio
async def test_restart(event_loop):
    manager = MessageManager(loop=event_loop)

    @Agg2Service.agg2_consumer.listen(Agg1Service.state_events)
    async def agg1_exist(id, event):
        if isinstance(event, Agg1Created):
            Agg2Service.agg1.add(id)
        elif isinstance(event, Agg1Deleted):
            Agg2Service.agg1.remove(id)

    await manager.start()
    await Agg1Service.create(agg1_id=1, a=1)
    obj = Agg1Service.aggregate_type.repo.load(1)
    assert obj.a == 1
    await asyncio.sleep(0.1)
    await manager.restart()
    await manager.stop()


@pytest.mark.asyncio
async def test_error(event_loop):
    manager = MessageManager(loop=event_loop)

    @Agg2Service.agg2_consumer.listen(Agg1Service.state_events)
    async def agg1_exist(id, event):
        raise ValueError(f"{id}, {event}")

    await manager.start()
    await Agg1Service.create(agg1_id=1, a=1)
    await Agg1Service.delete(agg1_id=1)
    obj = Agg1Service.aggregate_type.repo.load(1)
    assert obj.a == 1
    await asyncio.sleep(0.1)
    await manager.stop()


@pytest.mark.skip("experimenting the new message bus")
@pytest.mark.asyncio
async def test_consumer_task_multimethod_fn(event_loop, logger):
    from core.functools.multimethod import multimethod

    manager = MessageManager(loop=event_loop)
    publisher = Producer(id="testing-producer")

    @dataclass
    class A:
        a: int
        consumer = Consumer(id="testing-consumer", ismethod=True)

        @consumer.listen(publisher)
        @multimethod
        async def func(self, id: str, event: str):
            self.a += 1
            logger.info(f"got!, a={self.a}")

        @func.register
        async def func(self, id: str, event: int):
            self.a += event
            logger.info(f"got!, a={self.a}")

    a = A(a=0)

    await manager.start()
    await publisher.publish(id="sdf", event="event")
    await publisher.publish(id="sdf", event=3)
    await manager.stop()


@pytest.mark.skip("experimenting the new message bus")
@pytest.mark.asyncio
async def test_consumer_task_fn(event_loop, logger):
    from core.functools.multimethod import multimethod
    import mode
    import inspect
    manager = MessageManager(loop=event_loop)
    publisher = Producer(id="testing-producer")

    class B(mode.Service):
        def __init_subclass__(cls):
            super().__init_subclass__()
            cls.__init_subclass_consumer()

        @classmethod
        def __init_subclass_consumer(cls):
            cls._consumers = {}
            for attr_name, attr_value in vars(cls).items():
                if issubclass(type(attr_value), Consumer):
                    cls._consumers[attr_name] = attr_value

        def _get_tasks(self):
            for task in super()._get_tasks():
                yield task
            for consumer in self._consumers.values():
                for task in consumer.tasks:
                    yield task

    global_consumer = Consumer(id="global-testing-consumer")

    @global_consumer.listen(publisher)
    async def global_func(id, event):
        logger.info(f"global-func {id}, {event}")

    class A(B):
        a: int
        consumer = Consumer(id="testing-consumer", ismethod=True)

        def __init__(self, a, *arg, **kwargs):
            super().__init__(*arg, **kwargs)
            self.a = a

        @consumer.listen(publisher)
        @multimethod
        async def func(self, id: str, event: str):
            self.a += 1
            logger.info(f"got!, a={self.a}")

        @func.register
        async def _(self, id: str, event: int):
            self.a += event
            logger.info(f"got!, a={self.a}")

    a = A(a=0, loop=event_loop)
    a.add_dependency(manager)
    await a.start()
    await publisher.publish(id="sdf", event="event-strings")
    await publisher.publish(id="sdf", event=3)
    await a.stop()

@pytest.mark.asyncio
async def test_partial_effect():
    """
    do not include all the consumers and producers for listen effect.
    """
    manager = MessageManager(loop=event_loop)

    @Agg2Service.agg2_consumer.listen(Agg1Service.state_events)
    async def agg1_exist(id, event):
        raise ValueError(f"{id}, {event}")
    manager.clear() # clearn all consumer and producer
    manager.include_graph_from(
            Agg2Service.agg2_consumer) # include graph from a entry
    await manager.start()
    await Agg1Service.create(agg1_id=1, a=1)
    await Agg1Service.delete(agg1_id=1)
    obj = Agg1Service.aggregate_type.repo.load(1)
    assert obj.a == 1
    await asyncio.sleep(0.1)
    await manager.stop()



