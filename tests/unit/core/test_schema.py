"""
XXX: If we set frozen=True, that faust.Record will fail to
initial since, it using the code gen to compile the dataclass.
and set the attribute
TODO: Using typing.cast to achive this behavior

"""
import typing
from dataclasses import dataclass
import pytest
import abc
from pydantic import BaseModel
import unittest
from enum import Enum
from core.schema import pydantic_basemodel, get_derived, cast
from itertools import product

DataT = typing.TypeVar('DataT')


class MyMeta(typing.Generic[DataT], metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def my_method(self) -> DataT:
        ...


my_enum = Enum("my_enum", [("a", 1), ("cccc", 2)])

data = {
    "foo": {
        "a": 1,
        "b": "1",
        "e": 1
    },
    "foo_gen": [{
        "a": 2,
        "b": "2",
        "e": 2
    }],
    "int_list": [1, 2, 4],
    "c": "str"
}


@pydantic_basemodel
@dataclass
class Foo:
    a: int
    b: str
    e: my_enum


@pydantic_basemodel
@dataclass
class MyDs(MyMeta[Foo]):
    foo: Foo
    foo_gen: typing.List[Foo]
    int_list: typing.List[int]
    c: str
    d: int = 1

    def my_method2(self):
        return "method2"

    def my_method(self) -> Foo:
        return self.d


# reordered
@pydantic_basemodel
@dataclass
class MyTT:
    foo: Foo
    c: str
    d: int = 1

    def my_method2(self):
        return "method2"

    def my_method(self):
        return self.d

# Pydantic Model Declaration

class MyMetaBaseModel(typing.Generic[DataT], metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def my_method(self) -> DataT:
        ...


class _FooBaseModel(BaseModel):
    a: int
    b: str
    e: my_enum


class MyDsBaseModel(MyMetaBaseModel[_FooBaseModel], BaseModel):
    foo: _FooBaseModel
    foo_gen: typing.List[_FooBaseModel]
    int_list: typing.List[int]
    c: str
    d: int = 1

    def my_method2(self):
        return "method2"

    def my_method(self) -> _FooBaseModel:
        return self.foo


@pytest.fixture(scope="class")
def decorated_dataclass(request):
    """
    Case:
        Decorator Usuage Example
    """
    print(get_derived(MyDs, faust.Record).__annotations__)
    my_faust_record = get_derived(MyDs, faust.Record)
    request.cls.input_cls = MyDs
    request.cls.input_obj = MyDs(**data)
    request.cls.record = my_faust_record(**data)
    request.cls.expected_record = MyDsFaustRecord(**data)


@pytest.fixture(scope="class")
def basemodel_dataclass(request):
    """
    Case:
        Decorator Usuage Example
    """

    basemodel = get_derived(MyDs, BaseModel)
    request.cls.input_cls = MyDs
    request.cls.record = basemodel(**data)
    request.cls.expected_record = MyDsBaseModel(**data)


@pytest.mark.usefixtures("basemodel_dataclass")
class BaseModelTest(unittest.TestCase):
    """
    Test the class is reconize as BaseModel
    """
    def test_basemodel(self):
        assert isinstance(self.record, BaseModel)

    def test_nested(self):
        assert isinstance(self.record.foo, BaseModel)

    def test_faust_decorator_to_basemodel(self):
        basemodel = get_derived(MyTT, BaseModel)
        assert issubclass(basemodel, BaseModel)
        assert isinstance(basemodel(**data), BaseModel)


def test_type_cast():
    target_types = [BaseModel, MyDs]
    all_output_type = map(lambda x: cast(MyDs, x), target_types)
    combs = list(product(all_output_type, target_types))
    assert all(list(map(lambda x: issubclass(cast(x[0], x[1]), x[1]), combs)))


def test_basetype_cast():
    myds = cast(MyDs, BaseModel)
    args = typing.get_args(myds.__annotations__["foo_gen"])
    assert issubclass(args[0], BaseModel)
