import pytest

from core.command import (
    Command, command, is_registered, list_command_cls, get_command_cls)

import typing
from core.functools import singledispatchmethod
from unittest.mock import Mock
from dataclasses import dataclass


def cls_factory(name, base, namespace, **init_subclass_arg):
    """
    helper function
    """
    cls = type(name, base, namespace)
    cls.__init_subclass__(**init_subclass_arg)
    return dataclass(cls)


@pytest.fixture
def data():
    yield {"id": None, "a": 1, "b": 1}


@pytest.fixture
def register_command(data):
    from core.aggregate import Aggregate
    from core.events import Event
    from multimethod import multimethod
    base = (
        Aggregate,
    )

    init_subclass = {}
    annotation = {"a": int, "b": int}

    @dataclass
    class MyClasEvent(Event):
        obj_id: str
        a: int
        b: int

    @dataclass
    class MyFuncEvent(Event):
        a: int
        b: int

    @singledispatchmethod
    @classmethod
    def initial_apply(cls, event: typing.Any):
        raise NotImplementedError

    @initial_apply.register
    @classmethod
    def _(cls, event: MyClasEvent):
        return cls(id=event.obj_id, a=event.a, b=event.b)

    @singledispatchmethod
    def apply(self, event: typing.Any):
        raise NotImplementedError

    @apply.register
    def _(self, event: MyFuncEvent):
        self.b = event.a + event.b
        return self

    # This will register as command
    @command
    @classmethod
    def my_clsmethod(cls, obj_id: str,a: int, b: int):
        return MyClasEvent(obj_id=obj_id, a=a, b=b)

    @command
    def my_func(self, a: int, b: int):
        return MyFuncEvent(a=a, b=b)

    namespace = {
        "initial_apply": initial_apply,
        "apply": apply,
        "my_func": my_func,
        "my_clsmethod": my_clsmethod,
        "__annotations__": annotation
    }
    cls = cls_factory("A", base, namespace, **init_subclass)
    yield cls(**data)


def test_classmethod_command_and_method_command_execution(register_command):
    """
    classmethod command can be generated.
    """
    cls = register_command.__class__
    obj, event = cls.my_clsmethod(obj_id="sdf", a=1, b=1)
    obj2 = obj.my_func(a=1, b=1)
    _obj = cls.my_clsmethod.command_type(
        obj_id="sdf", a=1, b=1).execute(
        None, cls=cls)
    assert isinstance(obj, cls)
    assert obj.a == 1 and obj.b == 2


@pytest.fixture
def command_cls(register_command):
    """
    yield a command type
    """
    cls = register_command.__class__
    yield get_command_cls(cls.my_func)



def test_command_decorator(register_command):
    """
    Testing the command decorator to reigstery the command sorucing.
    Test the @register function is correctly function.
    1. need to be detected by the list_command_cls
    2. consist with faust.Record __init_subclass__ beahavior
    3. the get command cls will return a Command Class
    """
    cls = register_command.__class__
    assert set(list_command_cls(cls).keys()) == {"my_func", "my_clsmethod"}
