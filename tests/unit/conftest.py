import pytest


@pytest.fixture
def logger():
    from core.logger import logger
    yield logger
