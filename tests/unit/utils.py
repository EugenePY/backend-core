"""
Check Relation Test
Many-to-Many is Strongest -> one-to-Many -> one-to-one
"""
from typing import Tuple, List, Set, Any
from itertools import zip_longest
from unittest.mock import Mock
from asyncio import coroutine


def mock_coro(return_value=None, **kwargs):
    """Create mock coroutine function."""
    async def wrapped(*args, **kwargs):
        return return_value

    return Mock(wraps=wrapped, **kwargs)


def make_coroutine(mock):
    async def coroutine(*args, **kwargs):
        return mock(*args, **kwargs)

    return coroutine


def make_one_to_one(a_index: Set, b_index: Set) -> \
        List[Tuple[Any, Any]]:
    assert len(a_index) == len(b_index)
    return list(zip(a_index, b_index))


def make_many_to_many(a_index: Set, b_index: Set) -> \
        List[Tuple[Any, Any]]:
    idx = []
    for aidx in a_index:
        for bidx in b_index:
            idx.append((aidx, bidx))
    return idx


def make_one_to_many(a_index: Set, b_index: Set) -> \
        List[Tuple[Any, Any]]:
    """
    Generaly we only need to test there is one'one-to-many' relation.
    a_idx = [1, 2, 3],  b_idx = [1, 2, 3, 4]

    (1, 1)
    (2, 2)
    (3, 3)
    (1, 4)
    which is one-to-many
    (1, [1, 4])
    (2, [2])
    (3, [3])
    """
    assert len(b_index) >= len(a_index) + 1
    idx = list(map(lambda x: (x[0], x[1]), zip_longest(a_index, b_index)))
    return idx
