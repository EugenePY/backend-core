import pytest
from akira.core.logger import logger
from akira.services.positions.domain.user.aggregate import (
    UserAggregate, AccountCreated, AccountDeleted)
from akira.services.positions.domain.user.events import (
    UserCreated, UserDeleted, UserUpdated)


@pytest.fixture
def aggregate_type():
    yield UserAggregate


def test_user_create(aggregate_type, aggregate_apply):
    state, event = aggregate_type.create(user_id="sdfsdf",
                                         name="eugnepy",
                                         email="sdfa@gmail.com",
                                         password="sdfsdf")
    assert state.check_password("sdfsdf")
    assert isinstance(event, UserCreated)


def test_user_delete(aggregate_type, aggregate_apply):
    state, event = aggregate_type.create(user_id="sdfsdf",
                                         name="eugnepy",
                                         email="sdfa@gmail.com",
                                         password="sdfsdf")
    state, event = state.delete()
    assert state is None
    assert isinstance(event, UserDeleted)


def test_user_update(aggregate_type, aggregate_apply):
    state, event = aggregate_type.create(user_id="sdfsdf",
                                         name="eugnepy",
                                         email="sdfa@gmail.com",
                                         password="sdfsdf")
    new_email = "sdafsdfsdf@gamil.com"
    state, event = state.update_email(new_email=new_email)
    assert state is not None
    assert isinstance(event, UserUpdated)
    assert state.email == new_email


def test_user_add_account(aggregate_type, aggregate_apply):
    state, event = aggregate_type.create(user_id="sdfsdf",
                                         name="eugnepy",
                                         email="sdfa@gmail.com",
                                         password="sdfsdf")
    logger.debug(state)
    state, event = state.create_account(
        account_id="account_id", account_type="model")
    assert state is not None
    assert isinstance(event, AccountCreated)
    assert "account_id" in state.accounts


def test_user_delete_account(aggregate_type, aggregate_apply):
    state, event = aggregate_type.create(user_id="sdfsdf",
                                         name="eugnepy",
                                         email="sdfa@gmail.com",
                                         password="sdfsdf")

    state, event = state.create_account(
        account_id="account_id", account_type="model")
    new_state, event = state.delete_account(account_id="account_id")
    assert "account_id" not in new_state.accounts


def test_user_dump(aggregate_type, serialization_test_routine):
    state, event = aggregate_type.create(user_id="sdfsdf",
                                         name="eugnepy",
                                         email="sdfa@gmail.com",
                                         password="sdfsdf")
    assert serialization_test_routine(aggregate_type, state) == state
