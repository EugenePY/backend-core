"""
Testing the whole app, running all services, and test the basic functionality.
"""
import pytest
import asyncio
from akira.app import get_app


@pytest.fixture
@pytest.mark.asyncio
async def app(tmp_path):
    from akira.config import Settings
    setting = Settings(APP_ROOT=str(tmp_path))
    app = get_app(setting)
    await app.start()
    yield app
    await app.stop()


@pytest.fixture
def base_url(app):
    yield f"http://{app.webserver.host}:{app.webserver.port}"


@pytest.fixture
def wrap_coroutine():
    from functools import wraps, partial

    def wrap(func):
        @wraps(func)
        async def run(*args, loop=None, executor=None, **kwargs):
            if loop is None:
                loop = asyncio.get_event_loop()
            pfunc = partial(func, *args, **kwargs)
            return await loop.run_in_executor(executor, pfunc)

        return run

    return wrap
