import pytest
import asyncio
from akira.services.positions.scheduler.jobs.update_inv import (
    update_investing_dot_com)


@pytest.mark.asyncio
async def test_update_investing_dot_com(app):
    await update_investing_dot_com()
    await asyncio.sleep(5)
