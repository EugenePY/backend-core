import pytest
import asyncio
import datetime
from akira.client import AKIRAClient
from akira.core.logger import logger


@pytest.fixture
def async_client(app, wrap_coroutine):
    """
    transform the cleint into async, in order to start a seperated service to test.
    """
    from inspect import getmembers, ismethod
    client = AKIRAClient(host=app.config.host, port=app.config.port)

    for fn_name, method in getmembers(client, predicate=ismethod):
        if fn_name[:2] != "__":
            fn = wrap_coroutine(method)
            setattr(client, fn_name, fn)
    yield client


@pytest.fixture
def client():
    """
    transform the cleint into async, in order to start a seperated service to test.
    """
    from inspect import getmembers, ismethod
    client = AKIRAClient(host="0.0.0.0", port=5000)
    yield client


@pytest.mark.skip
@pytest.mark.asyncio
async def test_async_client(app, wrap_coroutine, async_client):
    import uuid
    client = async_client
    user_id = "ekow"

    res = await client.register(user_id=user_id, password="sdfsdf",
                                name="eugene", email="tdf@gmail.com")

    logger.info(res.content)
    res = await client.login(user_id=user_id, password="sdfsdf")

    logger.info(res.content)

    account_params = {"account_id": "mv",
                      "account_type": "model"}

    res = await client.create_account(account_id="mv", account_type="model")
    res = await client.submit_trade(account_id="mv",
                                    symbol="USDKRW INVT Cruncy",
                                    unit=10)
    res = await client.submit_trade(account_id="mv",
                                    symbol="USDJPY INVT Cruncy",
                                    unit=30)
    logger.info(res.json())

    trade_ids = (await client.list_trade_ids(account_id="mv")).json()
    logger.info(trade_ids)

    for trade_id in trade_ids:
        res_ = await client.reserve_trade(account_id="mv",
                                          trade_id=trade_id)
        logger.info(res_.json())

    for trade_id in trade_ids:
        res_ = await client.execute(
            account_id="mv", trade_id=trade_id, cost=10)
        logger.info(res_.json())

    pos = await client.get_position(account_id="mv")
    logger.info(pos.json())
    await client.update_eco_event(start=datetime.datetime(2020, 1, 5),
                                  end=datetime.datetime(2020, 1, 30),
                                  country_code=[25])

    await asyncio.sleep(1)


@pytest.mark.asyncio
async def test_client(client):
    import uuid
    user_id = "ekow"

    res = client.register(user_id=user_id, password="sdfsdf",
                          name="eugene", email="tdf@gmail.com")

    logger.info(res.content)
    res = client.login(user_id=user_id, password="sdfsdf")

    logger.info(res.json())

    account_params = {"account_id": "mv",
                      "account_type": "model"}

    res = client.create_account(account_id="mv", account_type="model")
    res = client.submit_trade(account_id="mv", symbol="USDKRW INVT Cruncy",
                              unit=10)
    logger.info(res.content)
    res = client.get_all_trades(account_id="mv")
    logger.info(res.content)
    client.update_eco_event(start=datetime.datetime(2020, 1, 5),
                            end=datetime.datetime(2020, 1, 30),
                            country_code=[25])
