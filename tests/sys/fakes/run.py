"""
this script will run a simple json server http server.
"""
import click
import uvicorn
from collections import defaultdict
import json
from logging import getLogger
data = defaultdict(dict)

logger = getLogger(__name__)

@click.command()
@click.argument("host")
@click.option("--port", "-p", default=2000)
@click.option("--json_file", "-j", default=None)
def main(host, port, json_file):
    from tests.sys.fakes.api import app, data
    if json_file is not None:
        with open(json_file, "r") as f:
            click.echo(f"load_json db  ={json_file}")
            loadded_data = json.load(f)
        data.update(loadded_data)
    uvicorn.run(app, host=host, port=int(port))


if __name__ == "__main__":
    main()
