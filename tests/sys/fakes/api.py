from collections import defaultdict
from fastapi import FastAPI

app = FastAPI()

data = defaultdict(dict)

@app.get("/")
def idx():
    return {"status": "running"}

@app.get("/{collection}/{id}")
def get_data(collection: str, id: str):
    return data[collection].get(id, None)

@app.post("/{collection}/{id}")
def dump_data(collection: str, id: str, data: dict):
    data[collection][id] = data
    return data[collection][id]
