"""
Future Feature: deploy using mode.
"""
from akira.core.stores.timeseries.base import TimeSeriesStore
from akira.services.positions.domain.execution.service import TradeExecutorService
import mode
import mode.loop
import asyncio
from uvicorn import Server, Config
from pymongo import MongoClient
from mode.utils.objects import cached_property
from .pubsub import MessageManager
from .pubsub.event_store import TinyDBEventStore

import typing
import functools
from tinydb import TinyDB, JSONStorage
from tinydb.middlewares import CachingMiddleware
from pydantic.json import pydantic_encoder
from arctic import Arctic, TICK_STORE
from akira.services.positions.scheduler import SchedulerService
from akira.services.positions.ws.service import WebSockerServer
from akira.core.aggregate import AggregateRegistery
from akira.core.stores.tinydb import tinydb_factory
from akira.core.stores.timeseries.arctic import ArcticTimeSeriesStore


class WebServer(mode.Service):
    def __init__(self, config, *arg, **kwargs):
        super().__init__(*arg, **kwargs)
        self.config = config
        self._server = Server(config=config)

    @property
    def host(self):
        return self.config.host

    @property
    def port(self):
        return self.config.port

    async def on_start(self):
        config = self.config
        if not config.loaded:
            config.load()

        create_protocol = functools.partial(
            config.http_protocol_class,
            config=config,
            server_state=self._server.server_state)

        if self.loop.is_closed:
            self.loop = asyncio.get_running_loop()

        self._srv = await self.loop.create_server(
            create_protocol,
            host=config.host,
            port=config.port,
            ssl=config.ssl,
            backlog=config.backlog,
        )
        protocol_name = "https" if config.ssl else "http"
        self.log.info(
            f'Serving on {protocol_name}://{config.host}:{config.port}.')

    async def on_thread_stop(self) -> None:
        if self._srv.is_serving:
            self.log.info(f'FastAPI-Server Stopped.')
            self._srv.close()

    async def on_stop(self) -> None:
        await self.on_thread_stop()


class AkiraService(mode.Service):
    """
    SourceSerivce
    """
    _mode = "deploy"

    def __init__(self, config, *arg, **kwargs):
        super().__init__(*arg, **kwargs)
        self.config = config

    def setup_aggregate_repo(self, db_factory, db, *arg, **kwargs):
        AggregateRegistery.set_repo_cls(db_factory, db=db, *arg, **kwargs)
        return self

    def on_init_dependencies(self) -> typing.List:
        self.log.info("initializing db")
        if self.config.is_deployment:
            self.db = TinyDB(self.config.tinydb_path,
                             storage=CachingMiddleware(JSONStorage),
                             indent=4,
                             sort_keys=True,
                             default=pydantic_encoder)
            self.setup_aggregate_repo(tinydb_factory, self.db)
        kwargs = {}
        try:
            if self.config.is_deployment:
                self.arctic_db = Arctic(MongoClient(self.config.MONGO_URI))
                self.arctic_db.initialize_library("SERIES",
                                                  lib_type=TICK_STORE)
                self.tick_store = self.arctic_db["SERIES"]
                ts_store_type = ArcticTimeSeriesStore
                kwargs = {"arctic_lib": self.arctic_db}
            else:
                raise Exception
        except Exception:
            self.logger.info("using memory time series store")
            ts_store_type = TimeSeriesStore
        self.series_table = type("SeriesTable", (ts_store_type, ), {},
                                 max_length=300,
                                 **kwargs)
        return [
            self.message_bus, self.webserver, 
            self.ws_server, 
            self.scheduler,
            self.execution
        ]

    @property
    def mode(self):
        return self.config.mode

    @mode.setter
    def mode(self, value):
        if value not in ["test", "deploy"]:
            raise ValueError
        else:
            self.config.mode = value

    @cached_property
    def execution(self):
        return TradeExecutorService(loop=self.loop)

    @cached_property
    def message_bus(self):
        manager = MessageManager(loop=self.loop)
        if self.config.is_deployment:
            self.event_store = TinyDBEventStore(db=self.db,
                                                message_manager=manager)
            self.event_store.finalize()
        return manager

    @cached_property
    def webserver(self):
        from akira.services.positions.endpoints import app
        config = Config(app=app, host=self.config.host, port=self.config.port)
        if not config.loaded:
            config.load()
        self.log.debug(f"loaded-app={config.loaded_app.app}")
        return WebServer(config=config, loop=self.loop)

    @cached_property
    def ws_server(self):
        return WebSockerServer(symbol_ids=self.config.INVSETING_DOT_COM)

    @cached_property
    def scheduler(self):
        return SchedulerService(loop=self.loop)

    async def on_started(self) -> None:
        self.log.info('Service started (hit ctrl+C to exit).')

    def reset_config(self, setting):
        self.config = setting

    async def on_stop(self):
        if self.config.is_deployment:
            self.db.close()
        await super().on_stop()
