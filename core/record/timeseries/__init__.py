"""
Define A timeseries Receiver, version controll a given timestamp.
"""
from __future__ import annotations
from core.functools import singledispatchmethod, functional

import uuid
from functools import reduce
import typing
import datetime
import copy
from .events import (
    IndexCreated, IndexDeleted, IndexUpdated, SeriesCreated, SeriesDeleted)
from .record import TimeIndexedRecord, SeriesId
from sortedcontainers import SortedDict
from dataclasses import dataclass
from core.entity import Entity


def walk(
    a: typing.Mapping, b: typing.Mapping
) -> typing.Generator[typing.Tuple[str, typing.Any, typing.Any], None, None]:
    """
    walk through key and key b, and a, return None
    if the dict doesn't have that key.
    """
    intercept = set(
        reduce(lambda x, y: x + y, map(lambda x: list(x.keys()), [a, b])))
    for field in intercept:
        yield field, a.get(field, None), b.get(field, None)


@dataclass
class TimeSeriesBatch(Entity):
    """
    Defines Event and Aggreagate
    """
    id: SeriesId
    data: SortedDict

    def __post_init__(self):
        self.data = SortedDict(self.data)

    @classmethod
    def create(cls):
        ...

    def delete(self):
        ...

    @staticmethod
    def query(idx, data) -> typing.Union[typing.Mapping, None, str, float]:
        return reduce(lambda x, y: x.get(y, None), data, idx)

    def diff(self, record: "TimeSeriesBatch") -> \
            typing.Generator[Event, None, None]:
        if self.id == record.id:
            a, b = self.data, record.data
            for idx, _a, _b in walk(a, b):
                yield self.check_event(idx, _a, _b)
        else:
            raise ValueError(f"Only can differ from same id={self.id}, "
                             f"got={record.id}")

    @staticmethod
    def check_event(idx, _a, _b):
        if _a != _b:
            if _a is None:
                return IndexCreated(timestamp=idx, value=_b)
            elif _b is None:
                return IndexDeleted(timestamp=idx)
            else:
                return IndexUpdated(
                    timestamp=idx, old_value=_a, new_value=_b)

    def diff_one(self, idx: datetime.datetime, value: TimeIndexecRecord):
        a = self.data.get(idx, None)
        return self.check_event(idx, a, value)

    @singledispatchmethod
    def apply(self, event: Event) -> "TimeSeriesBatch":
        raise NotImplementedError(f'{event} is not Implemented')

    @apply.register
    def _do_noting(self, event: None):
        return self

    @apply.register
    def _create(self, event: IndexCreated) -> "TimeSeriesBatch":
        return self.create_index(timestamp=event.timestamp, value=event.value)

    @apply.register
    def _delete(self, event: IndexDeleted) -> "TimeSeriesBatch":
        return self.delete_index(timestamp=event.timestamp)

    @apply.register
    def _update(self, event: IndexUpdated) -> "TimeSeriesBatch":
        return self.update_index(timestamp=event.timestamp,
                                 new_value=event.new_value,
                                 old_value=event.old_value)

    @functional
    def create_index(self, timestamp: datetime.datetime,
                     value: typing.Mapping) -> "TimeSeriesBatch":
        """
        {"a": "b"}, {"b": "c"} -> {"a": "b", "b":"c"}
        """
        current = self.data.get(timestamp, None)
        if current is None:
            self.data[timestamp] = value
        else:
            raise ValueError("Cannot Create Index, " +
                             f"Index already exisit value={current}")
        return self

    @functional
    def delete_index(self, timestamp: datetime.datetime) -> "TimeSeriesBatch":
        """
        {"a": "b"}, {"b": "c"} -> {"a": "b", "b":"c"}
        """
        self.data.pop(timestamp)
        return self

    @functional
    def update_index(self, timestamp: datetime.datetime,
                     old_value: typing.Any,
                     new_value: typing.Any) -> "TimeSeriesBatch":
        old_value = self.data.get(timestamp, None)
        if old_value == old_value:
            self.data[timestamp] = new_value
        return self
