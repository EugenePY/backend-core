from core.record import Record
from dataclasses import dataclass, field
import datetime
from core.entity import Id
from core.registery import Registery
from core.schema import cast
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder


class SeriesIdRegistery(Registery):
    ...


@dataclass
class TimeIndexedRecord(Record):
    timestamp: datetime.datetime

    def __post_init__(self):
        self.timestamp = self.timestamp.replace(
            tzinfo=datetime.timezone.utc)

    def as_tick(self):
        doc = self.as_jsonable(exclude_none=True)
        doc["index"] = self.timestamp
        return doc

    @classmethod
    def from_tick(cls, **data):
        data.pop("index")
        return cls.from_jsonable(**data)

    @classmethod
    def from_jsonable(cls, **data):
        schema = cast(cls, BaseModel)
        data = schema(**data).dict()
        return cls(**data)

    def as_jsonable(self, *arg, **kwarg):
        return self._as_jsonable(self, *arg, **kwarg)

    @staticmethod
    def _as_jsonable(*arg, **kwarg):
        return jsonable_encoder(*arg, **kwarg)


@dataclass(eq=False, frozen=True)
class SeriesId(Id):
    _id: str = field(hash=True)
    symbol: str
    field: str

    def __init_subclass__(cls, record_type=None, *args, **kwargs):
        super().__init_subclass__(*args, **kwargs)
        SeriesIdRegistery.register(cls)
        if record_type is not None:
            if issubclass(record_type, Record):
                cls.record_type = record_type
            else:
                raise ValueError("record_type should "
                                 f"be a subclass of {Record}")
    def __hash__(self):
        return hash((self._id,))

    def __eq__(self, other):
        return hash(self) == hash(other)

    @classmethod
    def _from_string(cls, idx_string):
        symbol, field = idx_string.split(":")
        return symbol, field

    @classmethod
    def _gen_id(cls, symbol: str, field: str):
        return f"{symbol}:{field}"

    @classmethod
    def from_string(cls, idx_string: str):
        symbol, field = cls._from_string(idx_string)
        return cls(_id=idx_string, symbol=symbol, field=field)

