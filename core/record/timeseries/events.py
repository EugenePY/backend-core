import typing
import datetime
from core.events import Event
from dataclasses import dataclass


@dataclass
class IndexCreated(Event):
    timestamp: datetime.datetime
    value: typing.Any


@dataclass
class IndexDeleted(Event):
    timestamp: datetime.datetime


@dataclass
class IndexUpdated(Event):
    timestamp: datetime.datetime
    old_value: typing.Any
    new_value: typing.Any


@dataclass
class SeriesCreated(Event):
    symbol: str
    field: str


@dataclass
class SeriesDeleted(Event):
    symbol: str
    field: str
