import uuid
from dataclasses import dataclass, field
import abc



@dataclass
class Index(metaclass=abc.ABCMeta):
    """
    index instance
    """
    _id: str

    @property
    def id(self):
        return self._id


@dataclass
class Record(metaclass=abc.ABCMeta):
    """
    Schema Registery Record
    name: as the module path
    """
    ...

