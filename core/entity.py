import uuid
from dataclasses import dataclass, field
import typing
import abc

from .schema import pydantic_basemodel
from .registery import Registery


class IdRegistery(Registery):
    ...


@pydantic_basemodel
@dataclass(eq=True, frozen=True)
class Id(metaclass=abc.ABCMeta):
    id: uuid.UUID = field(default_factory=uuid.uuid1)

    def __init_subclass__(cls) -> None:
        IdRegistery.register(cls)

    def __hash__(self):
        return hash(str(self))

    def __str__(self):
        return str(self.id)

@dataclass
class Entity(metaclass=abc.ABCMeta):
    id: Id
