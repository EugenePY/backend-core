"""
#TODO: This Part is becomming to messay pls, refactor.
"""

import inspect
import dataclasses
from dataclasses import is_dataclass
from pydantic import BaseModel
import typing


def _walk(root: type) -> type:
    """
    Walk through the type Graph.
    """
    # is node
    if inspect.isabstract(root):
        # next level
        for child in root.__subclasses__():
            for subclass in _walk(child):
                yield subclass
    else:
        # check is leaf
        yield root


T = typing.TypeVar('T', str, BaseModel)

TYPE_SLOT = {
    BaseModel: "__derived_pydantic__",
}


def get_origin(derived_type):
    return derived_type.__derived_from__


def cast(input_type: typing.Type, target_type: typing.Type) -> typing.Type:
    """
    Casting the input type into target type
    """
    if is_dataclass(input_type):
        # is a dataclass
        if is_dataclass(target_type):
            return input_type
        return get_derived(input_type, target_type)
    else:
        # is BaseModel or Faust
        origin = input_type.__derived_from__
        return cast(origin, target_type)


def get_derived(input_type: typing.Type, derived_type: typing.Type):
    try:
        return getattr(input_type, TYPE_SLOT[derived_type])
    except Exception as e:
        raise ValueError(f"{input_type} is not registered as {derived_type}.")


def _cast_ducktype(input_types: typing.Container[typing.Type],
                   target_type: typing.Type, _cast: typing.Callable):
    types = input_types.__class__()
    for input_type in input_types:
        if isinstance(input_type, typing.List):
            _type = _cast_ducktype(input_type, target_type, _cast)
        else:
            if is_dataclass(input_type):
                _type = _cast(input_type, target_type)
            else:
                _type = input_type
        types += input_types.__class__([_type])
    return types


def _cast_datacls_and_type(input_type, target_type, _cast):
    if is_dataclass(input_type):
        _type = _cast(input_type, target_type)
    else:
        _type = input_type
    return _type


def _cast_genertic_node(gen_node: typing.Type, target_type: typing.Type,
                        _cast: typing.Callable) -> typing.Type:
    if isinstance(gen_node, typing._GenericAlias):
        args = typing.get_args(gen_node)
        all_args = []
        for arg in args:
            _args = _cast_genertic_node(arg, target_type, _cast)
            all_args.append(_args)
        return gen_node.copy_with(tuple(all_args))
    else:
        return _cast_datacls_and_type(gen_node, target_type, _cast)


def _register(node: typing.Type['DataclassType'],
              target_type: typing.Type) -> typing.Type:
    """
    Tranform input dataclass into target type record.
    """

    name = node.__name__ + "Schema"
    namespace = {t.__name__: t for t in target_type.__subclasses__()}

    if name in set(namespace.keys()):
        return namespace[name]

    attrs = {}
    annotations = {}

    for field in dataclasses.fields(node):
        # setup annotations
        if is_dataclass(field.type):
            # check if the field is a dataclass
            field_type = get_derived(field.type, target_type)
        else:
            field_type = _cast_genertic_node(field.type, target_type,
                                             get_derived)
        annotations[field.name] = field_type

        # get the default value
        if hasattr(node, field.name):
            attrs[field.name] = getattr(node, field.name)

    attrs["__annotations__"] = annotations

    # get methods
    attrs.update(dict(inspect.getmembers(node, predicate=inspect.isfunction)))

    if hasattr(node, "Config"):
        attrs["Config"] = node.Config

    # we force pop __init__, pls use post_init instead
    attrs.pop('__init__')
    attrs.pop('__new__', None)

    # back pointer
    attrs["__derived_from__"] = node
    return _factory(name, target_type, attrs, input_type=node)


def _factory(name: str, target_type: typing.Type,
             attrs: typing.Mapping[str, typing.Any], input_type: typing.Type):
    """
    Adding GenericModel support in new version.
    """
    base = (target_type, )
    cls = type(name, base, attrs)
    return cls


def is_faust(node: type) -> bool:
    """
    Check the model is derived from
    """
    return False if getattr(node, TYPE_SLOT[faust.Record],
                            None) is None else True


def is_registered_as_basemodel(cls):
    return hasattr(cls, TYPE_SLOT[BaseModel])


def is_model(node: type) -> bool:
    """
    Defining project model as class with annotations
    """
    return inspect.isclass(node) and hasattr(node, '__annotations__')


# Faust Record Registery


def faust_dataclass(_cls=None, *arg, **kwargs) -> typing.Callable:
    """
    hide the original dataclass into subtype of faust.Record class.
    """
    def wrap(cls):
        faust_cls = _register(cls, faust.Record)
        setattr(cls, TYPE_SLOT[faust.Record], faust_cls)
        return cls

    if _cls is None:
        return wrap

    return wrap(_cls)


def pydantic_basemodel(_cls=None,
                       *arg,
                       **kwarg) -> typing.Union[typing.Callable, typing.Type]:
    def wrap(cls):
        base_cls = _register(cls, BaseModel)
        setattr(cls, TYPE_SLOT[BaseModel], base_cls)
        return cls

    if _cls is None:
        return wrap
    return wrap(_cls)


# dataclass dependency walk
def walk_dataclass(
        node: typing.Type,
        depth: int) -> typing.Mapping[typing.Type, typing.List[typing.Type]]:
    """
    Only walk from abstract class to abstract class.
    """
    ...


castT = typing.TypeVar("castT",
                       bound=typing.Callable[[typing.Type], typing.Type])


def functor(cast: typing.Optional[castT] = None):
    """
    Change the Annoation of a function
    """
    def wrapped_fn(fn: typing.Callable):
        sign = inspect.signature(fn)
        for param in sign.parameters:
            param.annoation = cast(param.annoation)
        return fn

    if cast is None:
        fn = cast
        return wrapped_fn(fn)

    return wrapped_fn
