import abc
import uuid
import typing
import datetime
from core.registery import Registery


class EventRegistery(Registery):
    ...


class Event(metaclass=abc.ABCMeta):
    """
    Make this as factory
    """
    _id: uuid.UUID = uuid.uuid1()

    def __init_subclass__(cls):
        EventRegistery.register(cls)

    @property
    def event_time(self):
        return datetime.datetime(1582, 10, 15) + datetime.timedelta(
            microseconds=self._id.time // 10)
