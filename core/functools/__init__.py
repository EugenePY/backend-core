from .singledispatch import singledispatchmethod, functional
from .multimethod import multimethod
