from inspect import signature
import typing
from functools import partial, reduce, update_wrapper, singledispatch, wraps
import copy


def unpack():
    ...


def curring(
    func: typing.Callable, *args: typing.Tuple[typing.Any],
    **kwargs: typing.Mapping[str, typing.Any]
) -> typing.Union[typing.Callable, typing.Any]:

    param_count = len(signature(func).parameters)
    if param_count >= len(args) + len(kwargs):
        func = partial(func, *args, **kwargs)
        if (len(signature(func).parameters)) == 0:
            return func()
        return func
    else:
        raise Exception(
            'error: unmatched parameter(s) for function: {}@{}'.format(
                func.__name__, id(func)))


# descriptor version
class singledispatchmethod:
    """single-dispatch generic method descriptor.

    supports wrapping existing descriptors and handles non-descriptor
    callables as instance methods.
    """

    def __init__(self, func):
        if not callable(func) and not hasattr(func, "__get__"):
            raise TypeError(f"{func!r} is not callable or a descriptor")

        self.dispatcher = singledispatch(func)
        self.func = func

    def register(self, cls, method=None):
        """generic_method.register(cls, func) -> func

        registers a new implementation for
        the given *cls* on a *generic_method*.
        """
        if hasattr(cls, "__func__"):
            setattr(cls, "__annotations__", cls.__func__.__annotations__)
        return self.dispatcher.register(cls, func=method)

    def __get__(self, obj, cls=None):
        def _method(*args, **kwargs):
            method = self.dispatcher.dispatch(args[-1].__class__)
            return method.__get__(obj, cls)(*args, **kwargs)
        _method.__isabstractmethod__ = self.__isabstractmethod__
        _method.register = self.register
        update_wrapper(_method, self.func)
        return _method

    @property
    def __isabstractmethod__(self):
        return getattr(self.func, '__isabstractmethod__', False)


def functional(method):
    @wraps(method)
    def wrapper(self, *args, **kwargs):
        copied_self = copy.deepcopy(self)
        return method(copied_self, *args, **kwargs)
    return wrapper
