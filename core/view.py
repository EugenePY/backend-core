from akira.core.aggregate import Aggregate, AggregateRegistery
from akira.core.registery import Registery
from akira.core.schema import is_registered_as_basemodel, cast
from pydantic import BaseModel
from dataclasses import asdict


class ViewRegistery(AggregateRegistery):
    ...


class View:
    def __init_subclass__(cls, repo=None):
        ViewRegistery.register(cls)
        if repo is None:
            repo = ViewRegistery.get_default_repo_class(cls)
        cls.repo = repo

    def as_schema(self):
        dict_ = asdict(self)
        if is_registered_as_basemodel(self):
            if not hasattr(self, "schema"):
                self.schema = cast(self, BaseModel)
            return self.schema(**dict_)
        else:
            return dict_

    @classmethod
    def loads(cls, data):
        schema = cast(cls, BaseModel)
        return cls(**schema(**data).dict())


