from mode.services import ServiceTask
import asyncio
import traceback
from akira.core.logger import logger

class ConsumerTask(ServiceTask):

    def __init__(self, func, producer, consumer, force_complete=False):
        self.func = func
        self.producer = producer
        self.consumer = consumer
        self.task_future = None
        self.force_complete = force_complete
        super().__init__(self.fetching)

    def execute(self, obj, *args, **kwargs):
        if obj is not None:
            return self.func(obj, *args, **kwargs)
        else:
            return self.func(*args, **kwargs)

    async def fetching(self, obj=None):
        try:
            while True:
                q = self.producer.subscribers[self.consumer]
                id, event = await q.get()
                logger.debug(
                    f"Consumer={self.consumer.id}, " +
                    f"producer-id={self.producer.id}, key={id}, " +
                    f"event={event}")
                try:
                    await self.execute(obj, id, event)
                    q.task_done()
                except Exception:
                    logger.fatal(traceback.format_exc())
                    if not self.force_complete:
                        q.task_done()
        except asyncio.CancelledError:
            logger.debug(f"conumer={self.consumer.id} is stopped.")
            return "stopped"

    def create_task(self):
        return asyncio.create_task(
            self.fetching(),
            name=f"fetching-task:<{self.consumer.id}:{self.producer.id}>")

    def run(self):
        if self.task_future is None:
            self.task_future = self.create_task()
            logger.debug(f"{self.task_future.get_name()} start.")
        else:
            logger.debug(f"{self.task_future.get_name()} is already started.")

    def close(self):
        ...

    def force_close(self):
        t = self.task_future
        if not self.task_future.done():
            logger.info(f"closing task={self.task_future.get_name()}")
            t.cancel()
            logger.info(f"task={self.task_future.get_name()} closed")
            self.task_future = None
            return t

