import mode
import functools
from uvicorn import Server
import asyncio


class WebServer(mode.Service):
    def __init__(self, config, *arg, **kwargs):
        super().__init__(*arg, **kwargs)
        self.config = config
        self._server = Server(config=config)

    @property
    def host(self):
        return self.config.host

    @property
    def port(self):
        return self.config.port

    async def on_start(self):
        config = self.config
        if not config.loaded:
            config.load()

        create_protocol = functools.partial(
            config.http_protocol_class,
            config=config,
            server_state=self._server.server_state)

        if self.loop.is_closed:
            self.loop = asyncio.get_running_loop()

        self._srv = await self.loop.create_server(
            create_protocol,
            host=config.host,
            port=config.port,
            ssl=config.ssl,
            backlog=config.backlog,
        )
        protocol_name = "https" if config.ssl else "http"
        self.log.info(
            f'Serving on {protocol_name}://{config.host}:{config.port}.')

    async def on_thread_stop(self) -> None:
        if self._srv.is_serving:
            self.log.info(
                f'FastAPI-Server Stopped.')
            self._srv.close()

    async def on_stop(self) -> None:
        await self.on_thread_stop()


