from functools import wraps
from inspect import signature


def _return_root(fn, *arg, **kwarg):
    from akira.app import root_service
    return fn(root_service, *arg, **kwarg)


def given_root(fn):
    """
    helper function for getting the main app, to prevent the recursive import.
    """

    def _new_fn(*arg, **kwarg):
        return _return_root(fn, *arg, **kwarg)

    func_signature = signature(fn)
    new_signature = func_signature.replace(
        parameters=list(func_signature.parameters.values())[1:])
    _new_fn.__signature__ = new_signature
    _new_fn.__annotations__ = {
        k: v.annotation
        for k, v in new_signature.parameters.items()
    }
    return _new_fn
