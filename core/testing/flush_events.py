from core.functools import multimethod
from core.entity import Id
from core.events import Event
from core.logger import logger
import pytest


@pytest.fixture
@pytest.mark.asyncio
async def message_manager(event_loop):
    from core.pubsub import MessageManager
    manager = MessageManager(loop=event_loop)
    await manager.start()
    yield manager
    await manager.stop()


async def flush_events(events, aggregate_types, consumers_and_producers,
                       streams):
    @multimethod
    async def system_apply(key: Id, event: Event):
        logger.debug(f"id={key},event={event} not applied")

    for fn in streams:
        system_apply.register(fn)

    for event in events:
        await system_apply(*event)
