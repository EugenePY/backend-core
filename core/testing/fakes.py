from akira.services.positions.domain.trade.aggregate import TradeAggregate
from unittest.mock import patch
from functools import wraps
from akira.core.aggregate import AggregateRegistery
from collections import OrderedDict
import pytest


@pytest.mark.asyncio
@pytest.fixture
async def root_service(app):
    def _get_test_app(fn, *arg, **kwarg):
        return fn(app, *arg, **kwarg)
    mocker.patch("core.services.utils._return_root",
                 side_effect=_get_test_app)
    await app.start()
    yield app
    await app.stop()

def _pathing_mock_maps(mock_maps: OrderedDict):
    def _decorator(fn):
        _fn = fn
        for dependency, side_effect in mock_maps.items():
            _fn = patch(dependency, side_effect)(_fn)
        return _fn

    return _decorator


def patching_aggregate(aggregate_types_mock_maps: OrderedDict):
    _mock_maps = OrderedDict()
    for agg_type, mock in aggregate_types_mock_maps.items():
        _mock_maps[AggregateRegistery.gen_id(agg_type)] = mock
    return _pathing_mock_maps(_mock_maps)


def patch_apply_and_publish(apply_mock=lambda x: x,
                            publish_mock=lambda agg_type, _id, event:
                            (_id, event)):
    """
    Patch the akira.core.command._apply_decorator,
        and akira.core.pubsub._publish_decorator
    """
    def _return_id_event(topic):
        def _wrapper(fn):
            @wraps(fn)
            async def _new_fn(*arg, **kwarg):
                agg_type, _id, event = await fn(*arg, **kwarg)
                # do not actualy publish the event
                return publish_mock(agg_type, _id, event)

            return _new_fn

        return _wrapper

    return _pathing_mock_maps(
        OrderedDict([("akira.core.command._apply_decorator", apply_mock),
                     ("akira.core.pubsub._publish_decorator", _return_id_event)
                     ]))


@pytest.fixture
def web():
    """
    patch the app
    """
    from akira.services.positions.endpoints import app
    from akira.services.positions.domain.execution.entities.batch import ExecutorTypes
    from akira.services.positions.domain.execution.aggregate import TradeExecutorAggregate
    import datetime
    patch_message_bus()
    start = datetime.datetime.now()
    end = start + datetime.timedelta(seconds=10)
    fake = TradeExecutorAggregate._create(start=start, end=end)
    fake._add("sdf", "sdf", 10, executor_type=ExecutorTypes.SimpleExecutor)
    TradeExecutorAggregate.dump(fake.id, fake)
    yield app
    TradeExecutorAggregate.reset()
