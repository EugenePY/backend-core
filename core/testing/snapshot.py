import pytest
import json
import os
from fastapi.encoders import jsonable_encoder


@pytest.fixture
def dump_snapshot(request, logger):

    def _fn(record_list, file_name):
        dir_path = os.path.dirname(request.fspath) + "/data"
        if not os.path.isdir(dir_path):
            logger.info(f"{dir_path} not found create one")
            os.mkdir(dir_path)
        file_path = os.path.join(dir_path, file_name)
        jsonable_list = [jsonable_encoder(record)
                         for record in record_list]
        with open(file_path, "w") as f:
            json.dump(jsonable_list, f, indent=4)

    return _fn


@pytest.fixture
def load_snapshot(request, logger):

    def _fn(file_name):
        dir_path = os.path.dirname(request.fspath) + "/data"
        file_path = os.path.join(dir_path, file_name)
        with open(file_path, "r") as f:
            jsonable_list = json.load(f)
        return jsonable_list

    return _fn


@pytest.fixture
def mocked_source():
    """
    This Fixture generate a fake, that using the snapshout.
    """
    ...
