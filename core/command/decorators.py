from functools import wraps


def apply(aggregate_type, current_state, event):
    """
    life cycle of a aggregate
    """
    if current_state is None:
        return aggregate_type.initial_apply(event)
    else:
        return current_state.apply(event)


def _apply_decorator(fn):
    @wraps(fn)
    async def wrapper(*arg, **kwargs):
        agg_type, id, event = await fn(*arg, **kwargs)
        apply_and_dump_to_repo(agg_type.repo, id, agg_type, event)
        return id, event
    return wrapper


def apply_and_dump_to_repo(repo, id, aggregate_type, event):
    current_state = repo.load(id)
    next_state = apply(aggregate_type, current_state, event)
    repo.dump(id, next_state)
    return id, event
