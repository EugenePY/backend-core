import abc
import typing
import inspect
from functools import wraps
from dataclasses import dataclass
from core.events import Event

from .decorators import apply
from ..registery import Registery

REG_TAG = "__registed_aggregate__"
TAG = "__registered_command__"


class CommandRegistery(Registery):
    @classmethod
    def is_registered(cls, func):
        return cls.get(cls.gen_id(func)) is not None


class CommandMeta(metaclass=abc.ABCMeta):
    def __init_subclass__(cls):
        CommandRegistery.register(cls)

    @abc.abstractmethod
    def execute(self, receiver: "Aggregate") -> typing.List[Event]:
        raise NotImplementedError

    @property
    def kwargs_values(self):
        """
        get the annotaion of the function
        """
        return {k: getattr(self, k) for k, v in self.__annotations__.items()}

    @staticmethod
    def get_annotations(fn):
        if not (inspect.isfunction(fn) or inspect.ismethoddescriptor(fn)):
            raise ValueError(f"{fn} reigster func is needed annotations.")
        if inspect.ismethoddescriptor(fn):
            fn = fn.__func__
        fn_signature = inspect.signature(fn)
        annotations = {
            k: v.annotation
            for k, v in fn_signature.parameters.items()
            if k not in ["self", "cls"]
        }
        return annotations

    @classmethod
    def get_namespace(cls, fn):
        """
        Get the annotaion of the method
        """
        annotations = cls.get_annotations(fn)

        @wraps(fn)
        def execute(self, obj, cls=None):
            kwargs = self.kwargs_values
            event = fn.__get__(obj, cls)(**kwargs)
            if cls is None:
                _cls = obj.__class__
            else:
                _cls = cls
            new_state = apply(_cls, obj, event)
            return new_state, event

        @wraps(fn)
        def _event(self, obj, cls=None):
            kwargs = self.kwargs_values
            event = fn.__get__(obj, cls)(**kwargs)
            return event

        namespace = {
            "execute": execute,
            "__annotations__": annotations,
            "get_event": _event
        }
        return namespace

    @classmethod
    def get_bases(cls, fn):
        return (cls, )

    @classmethod
    def get_name(cls, fn):
        if inspect.ismethoddescriptor(fn):
            fn = fn.__func__
        return ''.join([c.title() for c in fn.__name__.split("_")])

    @classmethod
    def factory(cls, fn):
        """
        function factory
        """
        namespace = cls.get_namespace(fn)
        base = cls.get_bases(fn)
        cls_name = cls.get_name(fn)
        command_type = type(cls_name + "Command", base, namespace)
        return command_type


class Command(CommandMeta):
    @classmethod
    def get_bases(cls, fn):
        return (cls, )

    @classmethod
    def factory(cls, fn, *arg, **kwarg):
        command_type = super().factory(fn)
        return dataclass(command_type)


class command:
    def __init__(self, fn: typing.Callable):
        self.fn = fn
        self.command_type = Command.factory(self.fn)

    def __get__(self, obj, _cls=None):
        self.obj = obj
        self._cls = _cls
        return self

    def __call__(self, *args, **kwargs):
        return self.command_type(*args, **kwargs).execute(self.obj, self._cls)

    def get_event(self, *args, **kwargs):
        return self.command_type(*args, **kwargs).get_event(
                self.obj, self._cls)



def is_registered(func):
    if hasattr(func, "command_type"):
        func = func.command_type
    return CommandRegistery.is_registered(func)


def list_command_cls(cls):
    functions = inspect.getmembers(cls)
    results = {}
    for name, func in functions:
        if isinstance(func, command):
            results[name] = getattr(cls, name)
    return results


def get_command_cls(func):
    return CommandRegistery.get(func)
