"""
Simple Pub-Sub Pattern, the publisher only have signle thread.
"""
import asyncio
import traceback
from core.logger import logger
from .manager import MessageManager
from mode.services import ServiceTask


class ConsumerTask(ServiceTask):
    def __init__(self, func, producer, consumer, force_complete=False):
        self.func = func
        self.producer = producer
        self.consumer = consumer
        self.task_future = None
        self.force_complete = force_complete
        super().__init__(self.fetching)

    def execute(self, obj, *args, **kwargs):
        if obj is not None:
            return self.func(obj, *args, **kwargs)
        else:
            return self.func(*args, **kwargs)

    async def fetching(self, obj=None):
        try:
            while True:
                q = self.producer.subscribers[self.consumer]
                id, event = await q.get()
                logger.debug(f"Consumer={self.consumer.id}, " +
                             f"producer-id={self.producer.id}, key={id}, " +
                             f"event={event}")
                try:
                    await self.execute(obj, id, event)
                    q.task_done()
                except Exception:
                    logger.fatal(traceback.format_exc())
                    if not self.force_complete:
                        q.task_done()
        except asyncio.CancelledError:
            logger.debug(f"conumer={self.consumer.id} is stopped.")
            return "stopped"

    def create_task(self):
        return asyncio.create_task(
            self.fetching(),
            name=f"fetching-task:<{self.consumer.id}:{self.producer.id}>")

    def run(self):
        if self.task_future is None:
            self.task_future = self.create_task()
            logger.debug(f"{self.task_future.get_name()} start.")
        else:
            logger.debug(f"{self.task_future.get_name()} is already started.")

    def close(self):
        ...

    def force_close(self):
        t = self.task_future
        if not self.task_future.done():
            logger.info(f"closing task={self.task_future.get_name()}")
            t.cancel()
            logger.info(f"task={self.task_future.get_name()} closed")
            self.task_future = None
            return t


class Consumer:
    def __init__(self, id, ismethod=False):
        self._id = id
        self.subscribed = set()
        self.tasks = []
        self.manger = MessageManager
        if not ismethod:
            self.manger.add_consumer(self)
        self.ismethod = ismethod

    @property
    def id(self):
        return self._id

    async def start(self, loop):
        self.should_stop = False
        for task in self.tasks:
            task.run()

    async def close(self):
        self.should_stop = True
        tasks = []
        for task in self.tasks:
            task.close()

    async def force_close(self):
        tasks = []
        for task in self.tasks:
            tasks.append(task.close())
        await asyncio.gather(*tasks, return_exceptions=True)

    def listen(self, producer):
        self.subcribe_to(producer)

        def wrapper(fn):
            task = ConsumerTask(fn, producer, self)
            self.tasks.append(task)
            return fn

        return wrapper

    def subcribe_to(self, publisher):
        self.subscribed.add(publisher)
        publisher.publish_to(self)

    def is_subcribe_to(self, publisher_set):
        return len(self.subscribed.intersection(publisher_set)) > 0

    @property
    def task_futures(self):
        return [task.fetching() for task in self.tasks]
