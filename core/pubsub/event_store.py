import typing
from core.events import EventRegistery
from abc import abstractmethod

from dataclasses import dataclass, field
from core.events import Event
from core.stores.base import Store
from core.logger import logger
from dataclasses import asdict
from mode.utils.objects import canoname
from functools import wraps
from .manager import MessageManager
from .producers import Topic
from ..registery import Registery


class EventSourcedPublisherRegistery(Registery):
    @classmethod
    def gen_id(cls, pub):
        return pub.id


@dataclass
class EventRecord:
    id: typing.Any
    producer_id: str
    event: Event
    offset: int
    acked: bool = False

    @property
    def timestamp(self):
        return self.event.event_time

    def acked_record(self):
        self.acked = True
        return self.acked


@dataclass
class SnapShot:
    store: Store
    offset: int


@dataclass
class EventStore:
    message_manager: MessageManager
    count: int = -1
    store: typing.List = field(default_factory=list)

    def finalize(self):
        pub_set = set()
        for pub in self.message_manager.publishers.values():
            if isinstance(pub, Topic):
                if not EventSourcedPublisherRegistery.is_registered(pub.id):
                    new_pub = self._wrap_publish(pub.id, pub.publish)
                    pub_set.add(pub.id)
                    setattr(pub, "publish", new_pub)
                    EventSourcedPublisherRegistery.register(pub)

    def _wrap_publish(self, pub_id, publish_fn):
        async def wrapped_publish(id, event):
            topic = publish_fn.__self__
            offset = await self.dump(topic.id, id=id, event=event)
            new_event = self.wrap_event(id, topic.id, event, offset)
            await publish_fn(id=id, event=new_event)

        setattr(wrapped_publish, "__from_producer__", pub_id)
        return wrapped_publish

    def _wrap_listen(self, listen):
        @wraps(listen)
        def _wrapper(producer):
            fetching_wp = listen(producer)
            consumer = listen.__self__

            @wraps(fetching_wp)
            def __wrapper(fn):
                @wraps(fn)
                async def new_fn(id, event):
                    result = await fn(id, event)
                    record = self.get_event_record(event)
                    self.acked_message(record.offset)
                    return result

                return fetching_wp(new_fn)

            return __wrapper

        return _wrapper

    @staticmethod
    def wrap_event(id, producer_id, event, offset):
        record = EventRecord(id=id,
                             producer_id=producer_id,
                             event=event,
                             offset=offset)
        setattr(event, "__EVENTRECORD__", record)
        return event

    @staticmethod
    def get_event_record(event):
        return event.__EVENTRECORD__

    def store_event(self, record):
        self.store.append(record)

    def load_event(self, offset):
        return self.store[offset]

    async def dump(self, producer_id, id, event):
        self.count += 1
        record = EventRecord(id=id,
                             producer_id=producer_id,
                             event=event,
                             offset=self.count)
        logger.debug(f"EventStore, dump record={record}")
        # dumping the event-record
        self.store_event(record)
        return self.count

    def acked_message(self, event):
        record = self.load_event(self.get_event_record(event).offset)
        result = record.acked_record()
        logger.debug(f"EventStore, acked record={record}")
        return result

    async def replay_topic(self, offset):
        for i in range(offset):
            record = self.load_event(i)
            await self.message_manager.publishers[producer_id].publish(
                record.id, record.event)
        return current

    @property
    def events(self):
        return [i.event for i in self.store]


class SerializedEventStore(EventStore):
    @abstractmethod
    def store_event(self, record) -> None:
        ...

    @abstractmethod
    def load_event(self, offset) -> EventRecord:
        ...


class TinyDBEventStore(SerializedEventStore):
    def __init__(self, db, *arg, **kwarg):
        super().__init__(*arg, **kwarg)
        self._tinydb = db
        self.db = db.table("event-store")

    def __iter__(self):
        for topic_id, producer in self.message_manager.publishers.items():
            if isinstance(producer, Topic):
                yield topic_id, producer.agg_type

    def store_event(self, record):
        data = asdict(record)
        _cls = record.event.__class__
        _cls_id = canoname(_cls)
        data["__event_type__"] = _cls_id
        return self.db.insert(data)

    def load_event(self, offset):
        data = self.db.get(doc_id=offset + 1)
        namespace = data.pop("__event_type__")
        data["event"] = EventRegistery.get(namespace)(**data["event"])
        return EventRecord(**data)
