from .base import Producer
from core.command import apply


class Topic(Producer):
    def __init__(self, id, agg_type):
        super().__init__(id=id)
        self.agg_type = agg_type

    def apply_event(self, id, event):
        current = self.agg_type.repo.load(id)
        next_ = apply(self.agg_type, current, event)
        self.agg_type.repo.dump(id, next_)

    @property 
    def events(self):
        return self.agg_type.event_set
