import typing
import asyncio
from asyncio import Queue
from core.logger import logger
from functools import wraps
from ..manager import MessageManager


class Producer:
    def __init__(self, id):
        self._id = id
        self.subscribers = {}
        MessageManager.add_publisher(self)

    def start(self, loop=None):
        if loop is not None:
            for k, v in self.subscribers.items():
                self.subscribers[k] = Queue(loop=loop)

    def publish_to(self, consumer):
        self.subscribers[consumer] = None

    @property
    def id(self):
        return self._id

    async def publish(self, id: typing.Any, event: typing.Any):
        for _, q in self.subscribers.items():
            if q is not None:
                await q.put((id, event))
        if not self.subscribers:
            logger.warning(
                f"Publisher=producer-id:{self.id}(do not have any subcribers), key={id}, event={event}."
            )
        else:
            if q is None:
                logger.warning(
                    f"Publisher=producer-id:{self.id}(not started), key={id}, event={event}."
                )
            else:
                logger.debug(
                    f"Publisher=producer-id:{self.id}, key={id}, event={event}."
                )

    async def publish_no_wait(self, id, event):
        self.q.put_nowait((id, event))

    async def force_stop(self):
        for s, q in self.subscribers.items():
            if q is not None:
                if not q.empty():
                    t = q.get_nowait()
                    q.task_done()
            else:
                logger.debug(f"{s.id} was never started.")

    async def stop_until_queue_empty(self):
        for s, q in self.subscribers.items():
            logger.debug(f"Wait for consumer={s.id}")
            if q is not None:
                logger.debug(f"consumer={s.id} was never started")
                await q.join()
            logger.debug(f"consumer={s.id} is empty")


class _publish_descriptor:
    """
    publish decorator, publish given function output using the producer
    """
    def __init__(self, producer):
        self.producer = producer

    def __call__(self, fn):
        self.fn = fn
        return self

    def __get__(self, obj, cls=None):
        async def _method(*arg, **kwargs):
            id, event = self.fn.__get__(obj, cls)(*arg, **kwargs)
            await self.producer.publish(id, event)
            return id, event

        return _method


def _publish_decorator(producer):
    def _wrapper(fn):
        @wraps(fn)
        async def _fn(*args, **kwargs):
            id, event = await fn(*args, **kwargs)
            await producer.publish(id, event)
            return id

        return _fn

    return _wrapper
