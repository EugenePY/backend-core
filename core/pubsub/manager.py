import mode
import typing
import importlib


class MessageManager(mode.Service):
    """
    Singletone:
        import my_topic # the consumer or current
        manager = MessageManger(loop=my_loop)
        await manager.start()
        await manager.stop()
    find out the dependency graph of given consumers and publishers.
    """
    consumers = {}
    publishers = {}
    __instance__ = None

    def __new__(cls, *args, **kwargs):
        """
        signletone
        """
        if cls.__instance__ is None:
            cls.__instance__ = super().__new__(cls, *args, **kwargs)
        return cls.__instance__

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    def clear(self):
        """
        clearn all consumer, and publisher
        """
        self.consumers = {}
        self.publishers = {}

    @classmethod
    def add_consumer(cls, consumer):
        cls.consumers[consumer.id] = consumer

    @classmethod
    def add_publisher(cls, publisher):
        cls.publishers[publisher.id] = publisher

    def start_publishers(self):
        for pub in self.publishers.values():
            pub.start(loop=self.loop)  # creating the queue
        self.log.info(f"producer={set([i for i in self.publishers.keys()])}")

    def start_consumers(self):
        cons = set()
        for con_name, con in self.consumers.items():
            for future in con.task_futures:
                self.add_future(future)
            cons.add(con_name)
        self.log.info(f"consumer={cons}")

    async def on_start(self):
        self.start_publishers()
        self.start_consumers()

    async def on_stop(self):
        self.log.info("Closing message bus")
        self.log.debug("Waiting for Empty Queue.")
        for i in self.publishers.values():
            await i.stop_until_queue_empty()
        self.log.debug("Stoping consumer.")
        #for s in self.consumers.values():
        #await s.close()


def get_message_bus(loop, include_topics: typing.List[str]):
    """
    In order to make to test the system easy, we test the sub system which define as the topics, the included topics will have pre-defined direced graph(pubsub pattern). the side effect will only affect the included topics.
    """
    # TODO: in order to patch the message bus we need the construct the graphical structure of Aggreagates command, we using the functional method to do this, currently the graphical structure is define in the endpoint, using the listen and publish.
    for topic in include_topics:
        importlib.import_module(topic)
    return MessageManager(loop=loop)
