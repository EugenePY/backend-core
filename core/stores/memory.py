from .base import Store


def memory_store_factory(agg_id, agg_type):
    return type(f"_{agg_type.__name__}MemoryStore", (MemoryStore,), {})


class MemoryStore(Store):
    def __init_subclass__(cls):
        super().__init_subclass__()
        cls._data = {}
        cls.size = 0

    @classmethod
    def dump(cls, _id, obj):
        if obj is not None:
            cls._data[_id] = obj
            cls.size += 1
        else:
            return cls.delete(_id)
        return _id

    @classmethod
    def load(cls, _id):
        return cls._data.get(_id, None)

    @classmethod
    def delete(cls, _id):
        result = cls._data.pop(_id)
        cls.size -= 1
        return result

    @classmethod
    def has(cls, _id):
        return cls.load(_id) is not None

    @classmethod
    def reset(cls):
        cls.__init_subclass__()

