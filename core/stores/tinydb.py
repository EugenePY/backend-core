from .base import Store
import json
from tinydb import where
from mode.utils.objects import canoname


def tinydb_factory(agg_id, agg_type, db):
    return type(f"_{agg_type.__name__}TinyDBStore", (TinyDBStore, ), {},
                db=db,
                aggregate_type=agg_type)


class TinyDBStore(Store):
    def __init_subclass__(cls, db, aggregate_type):
        super().__init_subclass__()
        cls._tinydb = db
        cls._db = db.table(canoname(aggregate_type))
        cls._agg_type = aggregate_type

    @classmethod
    def load(cls, id):
        json_doc = cls._db.search(where("id") == str(id))
        if json_doc:
            return cls._agg_type.loads(json_doc[-1])

    @classmethod
    def dump(cls, id, agg):
        json_doc = agg.as_schema().json() if agg is not None else None
        if json_doc is None:
            return cls._db.remove(where("id") == str(id))
        else:
            json_dict = json.loads(json_doc)
            return cls._db.upsert(json_dict, where("id") == str(id))

    @classmethod
    def has(cls, id):
        json_doc = cls._db.search(where("id") == str(id))
        return len(json_doc) > 0
