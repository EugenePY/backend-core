from .base import Store
from dataclasses import asdict
from importlib import import_module


class MongoORMStore(Store):
    def __init_subclass__(cls, aggregate_type=None, **kwarg):
        document = kwarg.pop("document", None)
        cls.document = document
        cls.aggregate_type = aggregate_type

    @classmethod
    def load_data(cls, _id):
        doc = cls.document.objects(id=_id)
        if doc:
            return doc.first().to_mongo().to_dict()

    @classmethod
    def load(cls, _id):
        data = cls.load_data(_id)
        data.pop("_id")
        return cls.aggregate_type(**data)

    @classmethod
    def dump(cls, obj, _id=None):
        data = asdict(obj)
        if _id is None:
            _id = cls.gen_id()
        obj = cls.document(id=_id, **data)
        return obj.save()


class PolyMorphicStore(MongoORMStore):
    @staticmethod
    def _get_type(module, name):
        return getattr(import_module(module), name)

    @classmethod
    def get_type(cls, _id):
        return cls.document.objects(id=_id).first()._type.get_type()

    @classmethod
    def serialize_type(cls, type_):
        module = type_.__module__
        name = type_.__qualname__
        return {"_module": module, "_name": name}

    @classmethod
    def load_data(cls, _id):
        data = super().load_data(_id)
        if data:
            _id = data.pop("_id")
            _type = data.pop("_type")
            return _id, _type, data

    @classmethod
    def load(cls, _id):
        result = cls.load_data(_id)
        if result:
            _id, _type, data = result
            _cls = cls.get_type(_id)
            return _cls(**data)

    @classmethod
    def dump(cls, _id, obj):
        data = asdict(obj)
        _constraint_type = cls.serialize_type(obj.__class__)
        data["_type"] = _constraint_type
        doc = cls.document(pk=_id, **data).save()
        return doc.id
