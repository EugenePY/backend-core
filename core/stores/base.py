import abc
import typing
import uuid
from mode.utils.objects import canoname
from core.registery import Registery


class StoreRegistery(Registery):
    ...


class Store(metaclass=abc.ABCMeta):
    def __init_subclass__(cls, *args, **kw):
        super().__init_subclass__(*args, **kw)
        StoreRegistery.register(cls)

    @abc.abstractclassmethod
    def dump(cls, _id, obj) -> typing.Any:
        ...

    @abc.abstractclassmethod
    def delete(cls_, _id) -> typing.Any:
        ...

    @abc.abstractmethod
    def load(cls, _id) -> typing.Any:
        ...

    @classmethod
    def has(cls, _id) -> typing.Any:
        ...

    @staticmethod
    def gen_id() -> str:
        return str(uuid.uuid1())

    @classmethod
    def diff(cls, _id, value):
        a = cls.load(_id)
        return a == value


class MultiIndexStore(Store):

    _data = {}

    @classmethod
    def load(cls, idx):
        return cls._load(idx, cls._data)

    @classmethod
    def dump(cls, idx, obj):
        cls._dump(idx, obj, cls._data)

    @classmethod
    def _load(cls, idx, data):
        for _id in idx:
            d = data.get(_id, None)
            if not isinstance(d, dict):
                return d
            else:
                data = d
        return d

    @classmethod
    def _dump(cls, idx, obj, data):
        if len(idx) == 1:
            data[idx[0]] = obj
        else:
            o = cls._load(idx[:-1], data)
            if o is None:
                o = {}
            o[idx[-1]] = obj
            data = cls._dump(idx[:-1], o, data)
        return data
