"""
Define A timeseries Receiver, version controll a given timestamp.
"""
from __future__ import annotations
from core.functools import singledispatchmethod, functional

from functools import reduce
import typing
import datetime
from core.events import Event
from core.record.timeseries.events import (
    IndexCreated, IndexDeleted, IndexUpdated, SeriesCreated, SeriesDeleted)
from core.record.timeseries.record import TimeIndexedRecord, SeriesId
from sortedcontainers import SortedDict
from dataclasses import dataclass, asdict
from core.entity import Entity
from pydantic import BaseModel
from core.schema import cast, pydantic_basemodel
import pandas as pd
import pydantic
import uuid
from core.logger import logger


def walk(
    a: typing.Mapping, b: typing.Mapping
) -> typing.Generator[typing.Tuple[str, typing.Any, typing.Any], None, None]:
    """
    walk through key and key b, and a, return None
    if the dict doesn't have that key.
    """
    intercept = set(reduce(lambda x, y: x + y,
                           map(lambda x: list(x.keys()), [a, b])))
    for field in intercept:
        yield field, a.get(field, None), b.get(field, None)


@pydantic_basemodel
@dataclass
class TimeIndexedRecordNotCompat(TimeIndexedRecord):
    record: typing.Mapping[str, typing.Any]

    def as_tick(self, *arg, **kwarg):
        doc = self._as_jsonable(self.record, exclude_none=True)
        doc["index"] = self.timestamp
        return doc


@dataclass
class TimeSeriesBatch:
    """
    Defines Event and Aggreagate
    """
    id: SeriesId
    data: SortedDict[datetime.datetime,
                     typing.Union[TimeIndexedRecord,
                                  TimeIndexedRecordNotCompat]
                     ]
    max_length: int = 1000

    def __post_init__(self):
        self.data = SortedDict(self.data)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]

    def popitem(self, *args, **kwargs):
        return self.data.popitem(*args, **kwargs)

    def diff(self, record: "TimeSeriesBatch") -> typing.Generator[
            Event, None, None]:
        if self.id == record.id:
            a, b = self.data, record.data
            for idx, _a, _b in walk(a, b):
                yield self.check_event(idx, _a, _b)
        else:
            raise ValueError(f"Only can differ from same id={self.id}, "
                             f"got={record.id}")

    @staticmethod
    def check_event(idx, _a, _b):
        if _a != _b:
            if _a is None:
                return IndexCreated(timestamp=idx, value=_b)
            elif _b is None:
                return IndexDeleted(timestamp=idx)
            else:
                return IndexUpdated(timestamp=idx,
                                    old_value=_a, new_value=_b)

    def diff_one(self, idx: datetime.datetime, value: TimeIndexedRecord):
        a = self.data.get(idx, None)
        return self.check_event(idx, a, value)

    @singledispatchmethod
    def apply(self, event: Event) -> "TimeSeriesBatch":
        raise NotImplementedError(f'{event} is not Implemented')

    @apply.register
    def _do_noting(self, event: None):
        return self

    @apply.register
    def _create(self, event: IndexCreated) -> "TimeSeriesBatch":
        return self.create_index(timestamp=event.timestamp, value=event.value)

    @apply.register
    def _delete(self, event: IndexDeleted) -> "TimeSeriesBatch":
        return self.delete_index(timestamp=event.timestamp)

    @apply.register
    def _update(self, event: IndexUpdated) -> "TimeSeriesBatch":
        return self.update_index(timestamp=event.timestamp,
                                 new_value=event.new_value,
                                 old_value=event.old_value)

    @functional
    def create_index(self, timestamp: datetime.datetime,
                     value: typing.Mapping) -> "TimeSeriesBatch":
        """
        {"a": "b"}, {"b": "c"} -> {"a": "b", "b":"c"}
        """
        current = self.data.get(timestamp, None)
        if current is None:
            # pop item when the max_length is excessed.
            if len(self) >= self.max_length:
                self.popitem(0)
            self.data[timestamp] = value
        else:
            raise ValueError("Cannot Create Index, " +
                             f"Index already exisit value={current}")
        return self

    @functional
    def delete_index(self, timestamp: datetime.datetime) -> "TimeSeriesBatch":
        """
        {"a": "b"}, {"b": "c"} -> {"a": "b", "b":"c"}
        """
        self.data.pop(timestamp)
        return self

    @functional
    def update_index(self, timestamp: datetime.datetime,
                     old_value: typing.Any,
                     new_value: typing.Any) -> "TimeSeriesBatch":
        old_value = self.data.get(timestamp, None)
        if old_value == old_value:
            self.data[timestamp] = new_value
        return self

    def as_records(self):
        return [i.as_jsonable() for i in self.data.values()]

    def as_dataframe(self):
        return pd.DataFrame.from_records(self.as_records(),
                                         index="timestamp")

    @staticmethod
    def _from_records_to_objs(records, schema):
        def _fn(**record):
            try:
                return schema(**record)
            except pydantic.error_wrappers.ValidationError:
                logger.info(record)
                record = TimeIndexedRecordNotCompat(
                    timestamp=record.pop("timestamp"),
                    record=record)
                return record
        return map(lambda record: _fn(**record), records)

    @classmethod
    def from_objs(cls, series_id: SeriesId,
                  objs: typing.List[TimeIndexedRecord], *arg, **kwargs):
        return cls(id=series_id, data={v.timestamp: v for v in objs},
                   *arg, **kwargs)

    @classmethod
    def from_records(cls, series_id: SeriesId,
                     records: typing.List[typing.Mapping],
                     schema: typing.Callable, *args, **kwargs):
        objs = cls._from_records_to_objs(records, schema)
        return cls.from_objs(series_id, objs, *args, **kwargs)

    @classmethod
    def from_dataframe(cls, series_id: SeriesId, df: pd.DataFrame,
                       schema: typing.Callable, *args, **kwargs) -> "TimeSeriesBatch":
        records = df.to_dict('records')
        objs = cls._from_records_to_objs(records, schema)
        return cls.from_objs(series_id, objs, *args, **kwargs)
