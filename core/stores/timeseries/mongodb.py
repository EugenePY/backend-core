from .base import TimeSeriesStore
import pymongo as mg
from core.record.timeseries import SeriesId


class MongoDBTimeSeriesStore(TimeSeriesStore):
    def __init_subclass__(cls, collection, *arg, **kwarg):
        super().__init_subclass__(*arg, **kwarg)
        cls.collection = collection
        cls.collection

    @classmethod
    def load(cls, series_id):
        batch = super().load(series_id)
        if batch is None:
            try:
                raise NotImplementedError
            except arctic.exceptions.NoDataFoundException:
                logger.debug(f"{series_id} not found")
                return None
        else:
            return batch

    @classmethod
    def write(cls, series_id: SeriesId, record: TimeIndexedRecord):
        ...
