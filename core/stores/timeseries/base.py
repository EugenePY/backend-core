import datetime
import typing
from core.record.timeseries import SeriesId, TimeIndexedRecord
from core.events import Event
from core.functools.multimethod import multimethod
from functools import reduce
from core.record.timeseries.events import (
    IndexCreated, IndexDeleted, IndexUpdated, SeriesCreated, SeriesDeleted)
from .entity import TimeSeriesBatch
from ..memory import MemoryStore


class TimeSeriesStore(MemoryStore):
    """
    TimeSeriesStore is the two key MultiIndex Store, first key is the id of
    SeriesId, second is the datetime Timestamp.

    {SeriesId._id: {datetime.datetime: value}}

    """
    def __init_subclass__(cls, max_length=10, *args, **kwargs):
        super().__init_subclass__(*args, **kwargs)
        cls.max_length = max_length

    @multimethod
    @classmethod
    def apply(cls, key: SeriesId, event: Event):
        raise NotImplementedError

    @apply.register
    def _(cls, key: SeriesId, event: SeriesCreated):
        cls._data[key] = TimeSeriesBatch(
            id=key, max_length=cls.max_length, data={})
        return cls

    @apply.register
    def _(cls, key: SeriesId, event: SeriesDeleted):
        cls._data.pop(key)
        return cls

    @apply.register
    def _(cls, key: SeriesId, event: typing.Union[IndexCreated, IndexUpdated, IndexDeleted]):
        entity = cls._data[key]
        entity = entity.apply(event)
        super().dump(key, entity)
        return cls

    @classmethod
    def diff(cls, key: SeriesId,
             value: TimeIndexedRecord) -> typing.Union[
                 Event, typing.List[Event]]:

        ts_batch = cls.load(key)
        timestamp = value.timestamp.replace(tzinfo=datetime.timezone.utc)
        if ts_batch is not None:
            return ts_batch.diff_one(timestamp, value)
        else:
            return [
                SeriesCreated(symbol=key.symbol, field=key.field),
                IndexCreated(timestamp=timestamp, value=value)
            ]

    @classmethod
    def dump(cls, key: SeriesId, value: TimeIndexedRecord):
        events = cls.diff(key, value)
        if isinstance(events, list):
            return reduce(lambda x, y: x.apply(key, y), events, cls)
        else:
            return cls.apply(key, events)

