from asyncio import Queue

class SerializedQueue(Queue):
    """
    Make the Queue is serialable.
    """
    ...
