from abc import ABCMeta
from mode.utils.objects import canoname
from inspect import isabstract


def is_abstract(input_cls):
    return isabstract(input_cls)


class Registery(metaclass=ABCMeta):
    def __init_subclass__(cls):
        cls._registery = {}

    @classmethod
    def gen_id(cls, _input_class):
        return canoname(_input_class)

    @classmethod
    def register(cls, _input_class):
        if not is_abstract(_input_class):
            cls._registery[cls.gen_id(_input_class)] = _input_class

    @classmethod
    def get(cls, _id):
        return cls._registery.get(_id, None)

    @classmethod
    def is_registered(cls, id):
        return id in cls._registery.keys()

    @classmethod
    def registered_cls(cls):
        return list(cls._registery.values())

    @classmethod
    def asdict(cls):
        return cls._registery
