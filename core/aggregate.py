import abc
import copy
from core.events import Event
from dataclasses import dataclass, asdict
from core.schema import is_registered_as_basemodel, cast
from pydantic import BaseModel
from .command import Command
from .registery import Registery
from .logger import logger
from .stores import MemoryStore
from .entity import Entity


class AggregateRegistery(Registery):

    @classmethod
    def get_default_repo_class(cls, agg_type):
        return type(f"_{agg_type}Store", (MemoryStore, ), {})

    @classmethod
    def set_repo_cls(cls, store_factory, *arg, **kwarg):
        for agg_id, agg_type in cls._registery.items():
            store = store_factory(agg_id, agg_type, *arg, **kwarg)
            logger.info(f"setup repo of {agg_id}, store={store}")
            agg_type.repo = store


@dataclass
class Aggregate(Entity, metaclass=abc.ABCMeta):
    def __init_subclass__(cls, repo=None, command_type=Command,
                          *arg, **kwarg):
        super().__init_subclass__(*arg, **kwarg)
        AggregateRegistery.register(cls)

        if repo is None:
            repo = AggregateRegistery.get_default_repo_class(cls)

        cls.repo = repo
        cls.build_events()
        cls._listen_from = {}

    def __post_init__(self):
        self.id = self.repo.gen_id() if self.id is None else self.id

    @property
    def listening_from(self):
        return self._listen_from

    @classmethod
    def build_events(cls):
        if hasattr(cls, "apply"):
            method_dispatcher = cls._get_dispatcher(cls.apply)
            events = dict(method_dispatcher.registry)

            if hasattr(cls, "initial_apply"):
                cls_dispatcher = cls._get_dispatcher(cls.initial_apply)
                cls_method_events = dict(cls_dispatcher.registry)
                events.update(cls_method_events)

            all_reigster_type = copy.deepcopy(list(events.keys()))
            not_event_types = [
                t for t in all_reigster_type if not issubclass(t, Event)
            ]
            for event in not_event_types:
                events.pop(event)

            cls.events = events
            cls.event_set = tuple(events.keys())

    @staticmethod
    def _get_dispatcher(dispatched_method):
        register = getattr(dispatched_method, "register", None)
        if register is not None:
            return register.__self__.dispatcher
        else:
            raise ValueError(
                f"{dispatched_method} is not used as a "
                "singledispatchmethod, ple use @functool"
                "s.singledispatchmethod")

    def as_schema(self):
        dict_ = asdict(self)
        if is_registered_as_basemodel(self):
            if not hasattr(self, "schema"):
                self.schema = cast(self, BaseModel)
            return self.schema(**dict_)
        else:
            return dict_

    @classmethod
    def loads(cls, data):
        schema = cast(cls, BaseModel)
        return cls(**schema(**data).dict())

    @classmethod
    def dump(cls, id, obj):
        cls.repo.dump(id, obj)

    @classmethod
    def load(cls, id):
        return cls.repo.load(id)

    @classmethod
    def reset(cls):
        cls.repo.reset()
