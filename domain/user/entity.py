import hashlib
import os
from dataclasses import dataclass
from core.entity import Entity
import attr


@dataclass
class User(Entity):
    id: str
    name: str
    email: str
    salt: str
    key: str
    role: str = "user"

    def __post_init__(self):
        super().__post_init__()
        if self.role not in {"user", "admin"}:
            raise ValueError(
                f"role should be either 'user' or 'admin' got {self.role}.")

    @property
    def is_encoded(self):
        if self.salt is not None or self.key is not None:
            return True
        return False

    @property
    def is_admin(self):
        if self.role == "admin":
            return True
        return False

    def check_password(self, password):
        if self.is_encoded:
            key = hashlib.pbkdf2_hmac(
                'sha256', password.encode('utf-8'),
                bytes.fromhex(self.salt), 100000).hex()
            return self.key == key
        return False

    @classmethod
    def get_secrete(cls, password):
        salt = os.urandom(32)
        key = hashlib.pbkdf2_hmac('sha256',
                                  password.encode('utf-8'), salt,
                                  100000)
        return key.hex(), salt.hex()

    def access_token(self, secrete_key):
        ...
