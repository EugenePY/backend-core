from dataclasses import dataclass
from core.events import Event


@dataclass
class UserCreated(Event):
    user_id: str
    name: str
    email: str
    key: str
    salt: str


@dataclass
class UserUpdated(Event):
    field: str
    new_value: str


@dataclass
class UserDeleted(Event):
    user_id: str



