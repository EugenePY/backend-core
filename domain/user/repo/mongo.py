from mongoengine import Document, fields


class UserMongo(Document):
    name = fields.StringField()
    email = fields.EmailField()
    password = fields.StringField()
    accounts = fields.ListField(fields.StringField())
