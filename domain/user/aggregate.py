"""
Event Driven Base CRUD
"""

import typing
from dataclasses import dataclass
from core.aggregate import Aggregate
from core.command import command
from core.functools import singledispatchmethod, functional
from core.schema import pydantic_basemodel

from .entity import User
from .repo import UserRepo
from .events import (UserCreated, UserUpdated, UserDeleted)


@pydantic_basemodel
@dataclass
class UserAggregate(User, Aggregate, repo=UserRepo):
    @singledispatchmethod
    @classmethod
    def initial_apply(cls, evnet: typing.Any):
        raise NotImplementedError

    @initial_apply.register
    @classmethod
    def _(cls, event: UserCreated):
        return cls(id=event.user_id,
                   name=event.name,
                   email=event.email,
                   key=event.key,
                   salt=event.salt)

    @singledispatchmethod
    def apply(self, event: typing.Any):
        raise NotImplementedError

    @apply.register
    @functional
    def _(self, event: UserUpdated):
        setattr(self, event.field, event.new_value)
        return self

    @apply.register
    @functional
    def _(self, event: UserDeleted):
        return None

    @command
    @classmethod
    def create(cls, user_id: str, name: str, email: str, password: str):
        if not cls.repo.has(user_id):
            key, salt = cls.get_secrete(password)
            return UserCreated(user_id=user_id,
                               name=name,
                               email=email,
                               key=key,
                               salt=salt)
        else:
            raise ValueError(f"user_id={user_id} already exist. ")

    @command
    def update_email(self, new_email: str):
        return UserUpdated(field="email", new_value=new_email)
