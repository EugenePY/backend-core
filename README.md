# Event-Driven Backend Framwork

Event Driven Backend Development kit

## Features

1. Event Bus
2. Time Versioned Data Structure
3. Tiny Event Store
4. Light weight Async Scheduler
5. Light weight Event Sourcing
